from setuptools import setup

setup(
    name="my_project",
    version="1.0.0",
    packages=["datalab"],
    include_package_data=True,
    entry_points={
        "console_scripts": [
            "DataLab = datalab.__main__:main"
        ]
    },
)