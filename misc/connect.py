import sys
import os
import traceback

from PyQt5.QtWidgets import *
from PyQt5.QtCore import *
from PyQt5.QtGui import *
import bleak
from bleak.backends.client import *
import asyncio
import time
import platform
import nest_asyncio
import pyqtgraph as pg

from hmon import *
from hmon.data.ShrdParser import ShrdParser
import matplotlib.pyplot as plt
from datetime import datetime

from typing import List
from collections import deque
from bitstring import BitArray

hmon_arg_split = "HMONARG"
sibel_ppg_stream_uuid = '05791401-78c9-481e-b5ee-1866f83eda7b'
sibel_ppg_CMD_uuid = '05791402-78c9-481e-b5ee-1866f83eda7b'
sibel_ecg_stream_uuid = '05791101-78c9-481e-b5ee-1866f83eda7b'
sibel_accl_stream_uuid = '05791301-78c9-481e-b5ee-1866f83eda7b'
sibel_bioz_stream_uuid = '05791701-78c9-481e-b5ee-1866f83eda7b'
sibel_temp_stream_uuid = '05791501-78c9-481e-b5ee-1866f83eda7b'
sibel_shrd_stream_uuid = '05791003-78c9-481e-b5ee-1866f83eda7b'
sibel_cmd_uuid = '05791002-78c9-481e-b5ee-1866f83eda7b'

sibel_uuid = {
    sibel_ppg_stream_uuid:'Sibel PPG Stream',
    '05791402-78c9-481e-b5ee-1866f83eda7b':'Sibel PPG CMD',
    sibel_temp_stream_uuid: 'Sibel Temp Stream',
    sibel_bioz_stream_uuid: 'Sibel BioZ Stream',
    sibel_ecg_stream_uuid: 'Sibel ECG Stream',
    sibel_accl_stream_uuid: 'Sibel Accl Stream',
    '05791901-78c9-481e-b5ee-1866f83eda7b': 'Sibel Security',
    '05791001-78c9-481e-b5ee-1866f83eda7b': 'Sibel System Stream',
    sibel_cmd_uuid: 'Sibel System CMD',
    sibel_shrd_stream_uuid: 'Sibel SHRD Stream',
    sibel_ppg_CMD_uuid: 'Sibel PPG CMD',
}
client_name = {}


if platform.system() == "Windows":
    desktop_dir = os.path.join("c:/", (os.environ["HOMEPATH"]), "Desktop")
else:
    desktop_dir = os.path.join(os.path.join(os.path.expanduser('~')), 'Desktop')


from hmon.ppg.PpgVitalSignAgent import PpgVitalSignAgent
from hmon.data.DataPacket import *
from hmon.settings.enum import *
ti_calib = np.array([-16.30622065,  -7.9177432 , 107.3604474 ])



class MainWindow(QMainWindow):
    def __init__(self):
        self.async_wakeupper = AsyncWakeup()
        self.async_wakeupper.signal.connect(self.wakeup)
        self.async_wakeupper.start()
        self.scan_device = False
        self.scanned_device_list = []
        self.connected_device_list = []
        self.running_clients = []

        super(MainWindow, self).__init__()
        self.hs = HmonSession(self)
        # Define the geometry of the main window
        self.setGeometry(0, 0, 1300, 1000)
        self.setWindowTitle("hMon Connect")
        # Create FRAME_A
        self.FRAME_MAIN = QFrame(self)
        # self.FRAME_MAIN.setStyleSheet("QWidget { background-color: %s }" % QColor(135, 206, 235,255).name())
        self.LAYOUT_MAIN = QHBoxLayout()
        self.FRAME_MAIN.setLayout(self.LAYOUT_MAIN)

        self.LAYOUT_COL1_INIT = QVBoxLayout()
        self.FRAME_COL1_INIT = QFrame(self.FRAME_MAIN)
        self.FRAME_COL1_INIT.setLayout(self.LAYOUT_COL1_INIT)
        self.col1_flo = QFormLayout()
        self.LAYOUT_COL1_INIT.addLayout(self.col1_flo)

        self.LAYOUT_COL2_DEVICE = QVBoxLayout()
        self.FRAME_COL2_DEVICE = QFrame(self.FRAME_MAIN)
        self.FRAME_COL2_DEVICE.setLayout(self.LAYOUT_COL2_DEVICE)
        self.col2_scanned_device_table = QTableWidget()
        self.col2_scanned_device_table.setColumnCount(3)
        self.col2_scanned_device_table.setRowCount(50)
        self.col2_scanned_device_table.setHorizontalHeaderLabels(["Name", "Rssi", "Address"])
        self.col2_scanned_device_table.setEditTriggers(QTableWidget.NoEditTriggers)
        self.col2_scanned_device_table.setSelectionBehavior(QAbstractItemView.SelectRows)
        self.col2_scanned_device_table.setSelectionMode(QAbstractItemView.SingleSelection)
        self.LAYOUT_COL2_DEVICE.addWidget(self.col2_scanned_device_table)
        self.col2_device_search = QLineEdit()
        self.col2_device_search.setPlaceholderText("Search Device")
        self.col2_device_search.textChanged.connect(self.deviceSearchAction)
        self.LAYOUT_COL2_DEVICE.addWidget(self.col2_device_search)
        self.col2_button_hbox = QHBoxLayout()
        self.LAYOUT_COL2_DEVICE.addLayout(self.col2_button_hbox)
        self.col2_button_scan = QPushButton("Scan (Ctrl+R)")
        self.col2_button_scan.setShortcut(QKeySequence("Ctrl+R"))
        self.col2_button_scan.clicked.connect(self.scanDeviceBtnAction)
        self.col2_button_connect = QPushButton("Connect (Ctrl+A)")
        self.col2_button_connect.setShortcut(QKeySequence("Ctrl+A"))
        self.col2_button_connect.clicked.connect(self.connectDeviceBtnAction)
        self.col2_scanned_device_table.doubleClicked.connect(self.connectDeviceBtnAction)
        self.col2_button_disconnect = QPushButton("Disconnect (Ctrl+D)")
        
        self.col2_button_disconnect.clicked.connect(self.disconnectDeviceBtnAction)
        self.col2_button_hbox.addWidget(self.col2_button_scan)
        self.col2_button_hbox.addWidget(self.col2_button_connect)
        self.col2_button_hbox.addWidget(self.col2_button_disconnect)

        self.LAYOUT_COL3_UUID = QVBoxLayout()
        self.FRAME_COL3_UUID = QFrame(self.FRAME_MAIN)
        self.FRAME_COL3_UUID.setLayout(self.LAYOUT_COL3_UUID)
        self.col3_connected_tree = QTreeWidget()
        self.col3_connected_tree.itemClicked.connect(self.scanServiceBtnAction)
        self.col3_connected_tree.setColumnCount(3)
        self.col3_connected_tree.setHeaderLabels(["Device Name", "Service", "Characteristic", "Description", "Properties"])
        self.col3_connected_tree.setSelectionBehavior(QAbstractItemView.SelectRows)
        self.col3_connected_tree.setSelectionMode(QAbstractItemView.SingleSelection)
        self.LAYOUT_COL3_UUID.addWidget(self.col3_connected_tree)

        self.col3_graph_table = QTableWidget(QSizePolicy.Preferred, QSizePolicy.Preferred)
        self.col3_graph_table.setColumnCount(7)
        len_channel = 16
        self.col3_graph_table.setRowCount(len_channel)
        self.col3_graph_table.setHorizontalHeaderLabels(["Channel Name", "Combo", "Bit Width", "Multiplier", "Length", "Signed", "Enable Graph"])
        self.clearGraphTable()
        self.col3_graph_table.setFocusPolicy(Qt.NoFocus)
        self.col3_graph_table.resizeColumnsToContents()
        self.col3_graph_table_btn =  QPushButton("Start Graph")
        self.col3_graph_table_btn.clicked.connect(self.startGraphBtnAction)
        self.col3_graph_table_clear_btn = QPushButton("Close Graph")
        self.col3_graph_table_clear_btn.clicked.connect(self.removeGraphBtnAction)
        self.LAYOUT_COL3_UUID.addWidget(self.col3_graph_table)
        self.LAYOUT_COL3_UUID.addWidget(self.col3_graph_table_btn)

        self.col3_notification_hbox = QHBoxLayout()
        self.LAYOUT_COL3_UUID.addLayout(self.col3_notification_hbox)
        self.col3_save_checkbox = QCheckBox("Log Data")
        self.col3_notification_hbox.addWidget(self.col3_save_checkbox)
        self.col3_button_toggle_notification = QPushButton("Enable Notification (Ctrl+T)")
        self.col3_button_toggle_notification.clicked.connect(self.toggleNotificationBtnAction)
        self.col3_button_toggle_notification.setShortcut(QKeySequence("Ctrl+T"))
        self.col3_notification_hbox.addWidget(self.col3_button_toggle_notification)
        
        self.col3_button_hbox = QHBoxLayout()
        self.LAYOUT_COL3_UUID.addLayout(self.col3_button_hbox)
        self.col3_forall_checkbox = QCheckBox("For All")
        self.col3_button_hbox.addWidget(self.col3_forall_checkbox)
        self.col3_send_input = QLineEdit()
        validator = QRegExpValidator(QRegExp("[0-9A-Fa-f_/ -]{0,}"))
        self.col3_send_input.setValidator(validator)
        self.col3_button_hbox.addWidget(self.col3_send_input)
        
        self.col3_button_send = QPushButton("Send Hex (Enter)")
        self.col3_button_send.setShortcut(QKeySequence("Return"))
        self.col3_button_send.clicked.connect(self.sendDataBtnAction)
        self.col3_button_hbox.addWidget(self.col3_button_send)

        self.col3_button_Special_hbox = QHBoxLayout()
        self.LAYOUT_COL3_UUID.addLayout(self.col3_button_Special_hbox)

        self.col3_sendLEDcurrent_input = QLineEdit()
        validator = QRegExpValidator(QRegExp("[0-9A-Fa-f_/ -]{0,}"))
        self.col3_sendLEDcurrent_input.setValidator(validator)
        self.col3_button_Special_hbox.addWidget(self.col3_sendLEDcurrent_input)

        self.col3_button_send_REDLED = QPushButton("Send R-LED 0-31mA")
        self.col3_button_send_REDLED.setShortcut(QKeySequence("Return"))
        self.col3_button_send_REDLED.clicked.connect(self.sendREDCurrentDataBtnAction)
        self.col3_button_Special_hbox.addWidget(self.col3_button_send_REDLED)

        self.col3_button_SPECIAL_CMD = QPushButton("Special Button")
        self.col3_button_SPECIAL_CMD.setShortcut(QKeySequence("Return"))
        self.col3_button_SPECIAL_CMD.clicked.connect(self.sendREDCurrentDataBtnAction)
        self.col3_button_Special_hbox.addWidget(self.col3_button_SPECIAL_CMD)

        self.col3_sendIRcurrent_input = QLineEdit()
        validator = QRegExpValidator(QRegExp("[0-9A-Fa-f_/ -]{0,}"))
        self.col3_sendIRcurrent_input.setValidator(validator)
        self.col3_button_Special_hbox.addWidget(self.col3_sendIRcurrent_input)

        self.col3_button_send_IR = QPushButton("Send IR 0-31mA")
        self.col3_button_send_IR.setShortcut(QKeySequence("Return"))
        self.col3_button_send_IR.clicked.connect(self.sendIRCurrentDataBtnAction)
        self.col3_button_Special_hbox.addWidget(self.col3_button_send_IR)

        self.col3_sibel_button_hbox = QHBoxLayout()
        self.LAYOUT_COL3_UUID.addLayout(self.col3_sibel_button_hbox)
        self.col3_button_start = QPushButton("Start Session (Ctrl+O)")
        self.col3_button_start.setShortcut(QKeySequence("Ctrl+O"))
        self.col3_button_start.clicked.connect(self.startBtnAction)
        self.col3_sibel_button_hbox.addWidget(self.col3_button_start)
        self.col3_button_stop = QPushButton("Stop Session (Ctrl+P)")
        self.col3_button_stop.setShortcut(QKeySequence("Ctrl+P"))
        self.col3_button_stop.clicked.connect(self.stopBtnAction)
        self.col3_sibel_button_hbox.addWidget(self.col3_button_stop)

        self.hideButtons()

        self.LAYOUT_COL4_GATT = QVBoxLayout()
        self.FRAME_COL4_GATT = QFrame(self.FRAME_MAIN)
        self.FRAME_COL4_GATT.setLayout(self.LAYOUT_COL4_GATT)
        self.col4_text_log = QTextEdit()
        # self.col4_text_log.NoWrap(True)
        self.col4_text_log.setLineWrapMode(False)
        self.LAYOUT_COL4_GATT.addWidget(self.col4_text_log)

        self.LAYOUT_MAIN.addWidget(self.FRAME_COL1_INIT)
        self.LAYOUT_MAIN.addWidget(self.FRAME_COL2_DEVICE)
        self.LAYOUT_MAIN.addWidget(self.FRAME_COL3_UUID)
        self.LAYOUT_MAIN.addWidget(self.FRAME_COL4_GATT)
        self.setCentralWidget(self.FRAME_MAIN)

        self.show()
        return

    def wakeup(self, arg):
        # print(arg)
        loop = asyncio.get_event_loop()
        if(not loop.is_running()):
            loop.run_until_complete(wakeup())
        pass

    def deviceSearchAction(self):
        self.col2_scanned_device_table.clear()
        self.scanned_device_list.clear()
        i = 0
        for device_name in self.hs.scanned_device_list.keys():
            device = self.hs.scanned_device_list[device_name]
            if (self.col2_device_search.text() in device.name and device.name != "Unknown"):
                self.col2_scanned_device_table.setItem(i, 0, QTableWidgetItem(device.name))
                self.col2_scanned_device_table.setItem(i, 1, QTableWidgetItem(str(device.rssi)))
                self.col2_scanned_device_table.setItem(i, 2, QTableWidgetItem(device.address))
                i += 1
                self.scanned_device_list.append(device)
        if(i != 0):
            self.col2_scanned_device_table.resizeColumnsToContents()
        self.col2_scanned_device_table.setHorizontalHeaderLabels(["Name", "Rssi", "Address"])

    def refresh_connected(self):
        global client_name
        self.col3_connected_tree.clear()
        self.connected_device_list.clear()
        i = 0
        for device_address in self.hs.connected_device_list.keys():
            client : BaseBleakClient
            client = self.hs.connected_device_list[device_address]
            client_item = QTreeWidgetItem(self.col3_connected_tree)
            client_item.setText(0, client_name[client.address])
            client_item.setExpanded(True)
            client_item.setData(10, 0, client)
            for cs in client.services:
                service = QTreeWidgetItem(client_item)
                service.setText(1, cs.uuid)
                service.setExpanded(True)
                service.setData(10, 0, client)
                for cc in cs.characteristics:
                    characteristic = QTreeWidgetItem(service)
                    characteristic.setText(2, cc.uuid)
                    if(cc.description == 'Unknown' and cc.uuid in sibel_uuid.keys()):
                        characteristic.setText(3, sibel_uuid[cc.uuid])
                    else:
                        characteristic.setText(3, cc.description)
                    characteristic.setText(4, str(cc.properties))
                    characteristic.setExpanded(True)
                    characteristic.setData(10, 0, client)
            i += 1

    def clearGraphTable(self):
        for i in range(self.col3_graph_table.rowCount()):
            for j in range(self.col3_graph_table.columnCount()):
                self.col3_graph_table.setItem(i, j, QTableWidgetItem())
            self.col3_graph_table.setItem(i, 5, get_checkbox())
            self.col3_graph_table.setItem(i, 6, get_checkbox())

    def scanDeviceBtnAction(self):
        loop = asyncio.get_event_loop()
        devices = loop.run_until_complete(scanDevices())
        self.hs.scan_devices(devices)
        self.deviceSearchAction()

    def connectDeviceBtnAction(self):
        print('Connecting')
        global client_name
        row = self.col2_scanned_device_table.currentRow()
        if(row > -1 and row < len(self.scanned_device_list)):
            device = self.scanned_device_list[row]
            loop = asyncio.get_event_loop()
            client, success = loop.run_until_complete(connectDevices(device, self.disconnectCallbackAction, True))
            if(success):
                client_name[client.address] = device.name
                self.hs.connected_device_add(client)
                self.refresh_connected()

    def disconnectDeviceBtnAction(self):
        col = self.col3_connected_tree.currentColumn()
        if(col > -1):
            it: QTreeWidgetItem
            it = self.col3_connected_tree.currentItem()
            client: BLEClient = it.data(10, 0)
            loop = asyncio.get_event_loop()
            loop.run_until_complete(disconnectDevices(client))
            self.refresh_connected()

    def disconnectCallbackAction(self, client):
        self.hs.connected_device_remove(client)
        self.refresh_connected()
        self.hideButtons()

    def hideButtons(self):
        self.col3_button_toggle_notification.hide()
        self.col3_button_send.hide()
        self.col3_save_checkbox.hide()
        self.col3_send_input.hide()
        self.col3_button_start.hide()
        self.col3_button_stop.hide()
        self.col3_graph_table.hide()
        self.col3_graph_table_btn.hide()
        self.col3_forall_checkbox.hide()
        self.col3_button_send_REDLED.hide()
        self.col3_button_send_IR.hide()
        self.col3_sendIRcurrent_input.hide()
        self.col3_sendLEDcurrent_input.hide()
        self.col3_button_SPECIAL_CMD.hide()

    def startGraphBtnAction(self):
        graph_infos = []
        try:
            for i in range(self.col3_graph_table.rowCount()):
                channel_name = self.col3_graph_table.item(i, 0).text()
                if(channel_name != ""):
                    combo = int(self.col3_graph_table.item(i, 1).text())
                    bit_width = int(self.col3_graph_table.item(i, 2).text())
                    multiplier = float(self.col3_graph_table.item(i, 3).text())
                    length = int(self.col3_graph_table.item(i, 4).text())
                    signed = self.col3_graph_table.item(i, 5).checkState() == Qt.Checked
                    graph = self.col3_graph_table.item(i, 6).checkState() == Qt.Checked
                    graph_infos.append(GraphInfo(channel_name, combo, bit_width, multiplier, length, signed, graph))
        except:
            return

        col = self.col3_connected_tree.currentColumn()
        it: QTreeWidgetItem
        it = self.col3_connected_tree.currentItem()
        client: BaseBleakClient = it.data(10, 0)
        uuid = it.text(2)
        self.hs.addGraphSession(client, uuid, graph_infos)

    def removeGraphBtnAction(self):
        col = self.col3_connected_tree.currentColumn()
        it: QTreeWidgetItem
        it = self.col3_connected_tree.currentItem()
        client: BaseBleakClient = it.data(10, 0)
        uuid = it.text(2)
        self.hs.removeGraphSession(client, uuid)

    def scanServiceBtnAction(self, it, col):
        col = self.col3_connected_tree.currentColumn()
        uuid = it.text(2)
        self.hideButtons()
        if (uuid != ''):
            it: QTreeWidgetItem
            it = self.col3_connected_tree.currentItem()
            client: BaseBleakClient = it.data(10, 0)
            characteristic = client.services.get_characteristic(uuid)
            if("notify" in characteristic.properties):
                is_notifying = False
                if (client.address in self.hs.running_clients.keys() and uuid in self.hs.running_clients[client.address].keys()):
                    handler: notificationAgent = self.hs.running_clients[client.address][uuid]
                    is_notifying = handler.is_notifying
                if(is_notifying):
                    self.col3_button_toggle_notification.setText("Disable Notification (Ctrl+T)")
                else:
                    self.col3_button_toggle_notification.setText("Enable Notification (Ctrl+T)")
                    self.col3_save_checkbox.show()
                self.col3_button_toggle_notification.show()
                self.col3_button_toggle_notification.setShortcut(QKeySequence("Ctrl+T"))
                self.col3_graph_table.show()
                self.col3_graph_table_btn.show()
            if("write" in characteristic.properties or "write-without-response" in characteristic.properties):
                self.col3_button_send.show()
                self.col3_send_input.show()
                self.col3_forall_checkbox.show()
                if(uuid == sibel_cmd_uuid):
                    self.col3_button_start.show()
                    self.col3_button_stop.show()
                # Check if characteristic includes PPG IR and REDLED current stuff
                if(uuid == sibel_ppg_CMD_uuid):
                    self.col3_button_send_REDLED.show()
                    self.col3_sendLEDcurrent_input.show()
                    self.col3_button_send_IR.show()
                    self.col3_sendIRcurrent_input.show()


        if(uuid in sibel_stream_uuid_graph.keys()):
            self.clearGraphTable()
            preset: List[GraphInfo] = sibel_stream_uuid_graph[uuid]
            for i, gi in enumerate(preset):
                self.col3_graph_table.item(i, 0).setText(gi.channel_name)
                self.col3_graph_table.item(i, 1).setText(str(gi.combo))
                self.col3_graph_table.item(i, 2).setText(str(gi.bit_width))
                self.col3_graph_table.item(i, 3).setText(str(gi.multiplier))
                self.col3_graph_table.item(i, 4).setText(str(gi.length))
                self.col3_graph_table.item(i, 5).setCheckState(Qt.Checked if gi.is_signed else Qt.Unchecked)
                self.col3_graph_table.item(i, 6).setCheckState(Qt.Checked if gi.is_graph_enabled else Qt.Unchecked)

    def toggleNotificationBtnAction(self):
        is_data_log_checked = self.col3_save_checkbox.isChecked()
        col = self.col3_connected_tree.currentColumn()
        it: QTreeWidgetItem
        it = self.col3_connected_tree.currentItem()
        client: BaseBleakClient = it.data(10, 0)
        uuid = it.text(2)
        if(uuid != ''):
            characteristic = client.services.get_characteristic(uuid)
            ret = self.hs.toggleNotification(client, uuid, is_data_log_checked)
            if (ret == 1):
                self.col3_button_toggle_notification.setText("Disable Notification (Ctrl+T)")
                self.col3_save_checkbox.hide()
            elif (ret == 0):
                self.col3_button_toggle_notification.setText("Enable Notification (Ctrl+T)")
                self.col3_save_checkbox.show()
            self.col3_button_toggle_notification.setShortcut(QKeySequence("Ctrl+T"))
    
    def sendDataBtnAction(self):
        col = self.col3_connected_tree.currentColumn()
        it: QTreeWidgetItem
        it = self.col3_connected_tree.currentItem()
        client: BaseBleakClient = it.data(10, 0)
        uuid = it.text(2)
        data = self.col3_send_input.text()
        data = data.replace("_", "")
        data = data.replace("/", "")
        data = data.replace("-", "")
        data = data.replace(" ", "")
        if(uuid != ''):
            data = bytearray.fromhex(data)
            if (self.col3_forall_checkbox.checkState() == Qt.Checked):
                send_all(self.hs, uuid, data)
            else:
                send_data(self.hs, client, uuid, data)

    # Callback for sending RED LED current button
    def sendREDCurrentDataBtnAction(self):
        it: QTreeWidgetItem
        it = self.col3_connected_tree.currentItem()
        client: BaseBleakClient = it.data(10, 0)
        uuid = it.text(2)
        data = self.col3_sendLEDcurrent_input.text()
        data = data.replace("_", "")
        data = data.replace("/", "")
        data = data.replace("-", "")
        data = data.replace(" ", "")
        if(uuid != ''):
            data = bytearray([(int)(0x23), min([(int)((float(data)/31)*255),255])])
            if (self.col3_forall_checkbox.checkState() == Qt.Checked):
                send_all(self.hs, uuid, data)
            else:
                send_data(self.hs, client, uuid, data)

    # Callback for sending IR current button
    def sendIRCurrentDataBtnAction(self):
        it: QTreeWidgetItem
        it = self.col3_connected_tree.currentItem()
        client: BaseBleakClient = it.data(10, 0)
        uuid = it.text(2)
        data = self.col3_sendIRcurrent_input.text()
        data = data.replace("_", "")
        data = data.replace("/", "")
        data = data.replace("-", "")
        data = data.replace(" ", "")
        if(uuid != ''):
            data = bytearray([(int)(0x24), min([(int)((float(data)/31)*255),255])])
            if (self.col3_forall_checkbox.checkState() == Qt.Checked):
                send_all(self.hs, uuid, data)
            else:
                send_data(self.hs, client, uuid, data)

    def startBtnAction(self):
        col = self.col3_connected_tree.currentColumn()
        it: QTreeWidgetItem
        it = self.col3_connected_tree.currentItem()
        client: BaseBleakClient = it.data(10, 0)
        uuid = it.text(2)
        if(uuid == sibel_cmd_uuid):
            data = bytearray.fromhex("01") + int(time.time()).to_bytes(4, 'little')
            if (self.col3_forall_checkbox.checkState() == Qt.Checked):
                send_all(self.hs, uuid, data)
            else:
                send_data(self.hs, client, uuid, data)

    def stopBtnAction(self):
        col = self.col3_connected_tree.currentColumn()
        it: QTreeWidgetItem
        it = self.col3_connected_tree.currentItem()
        client: BaseBleakClient = it.data(10, 0)
        uuid = it.text(2)
        if(uuid == sibel_cmd_uuid):
            data = bytearray.fromhex("02")
            if(self.col3_forall_checkbox.checkState() == Qt.Checked):
                send_all(self.hs, uuid, data)
            else:
                send_data(self.hs, client, uuid, data)
        # Pop up to download shrd file
        msg = QMessageBox()
        msg.setWindowTitle("File Download")
        msg.setText("Upload shrd?")
        msg.setStandardButtons(QMessageBox.Yes | QMessageBox.No)
        msg.setDefaultButton(QMessageBox.No)
        msg.buttonClicked.connect(self.FileDownloadPopupClick)
        x = msg.exec_()  # this will show our messagebox

    # Handler for SHRD auto-download
    def FileDownloadPopupClick(self, button:QAbstractButton):
        text = button.text()
        if (text == '&Yes'):
            #dirname = QFileDialog.getExistingDirectory(self, 'Select Folder/Directory', desktop_dir, QFileDialog.ShowDirsOnly)
            
            it: QTreeWidgetItem
            it = self.col3_connected_tree.currentItem()
            uuid = it.text(2)
            client: BaseBleakClient = it.data(10, 0)
            
            self.col3_save_checkbox.setChecked(True)
            is_data_log_checked = self.col3_save_checkbox.isChecked()
            uuidStream = sibel_shrd_stream_uuid

            if (client.address not in self.hs.running_clients.keys()):
                ret = self.hs.toggleNotification(client, uuidStream, is_data_log_checked)
                if (ret == 1):
                    self.col3_button_toggle_notification.setText("Disable Notification (Ctrl+T)")
                    self.col3_save_checkbox.hide()
            elif (not(self.hs.running_clients[client.address][uuidStream].is_notifying)):                
                ret = self.hs.toggleNotification(client, uuidStream, is_data_log_checked)
                if (ret == 1):
                    self.col3_button_toggle_notification.setText("Disable Notification (Ctrl+T)")
                    self.col3_save_checkbox.hide()

            self.hs.running_clients[client.address][uuidStream].ParsingSHRD = True
            if(uuid == sibel_cmd_uuid):
                data = bytearray.fromhex("04") # Request SHRD download
            if(self.col3_forall_checkbox.checkState() == Qt.Checked):
                send_all(self.hs, uuid, data)
            else:
                send_data(self.hs, client, uuid, data)

class HmonSession:
    def __init__(self, main_window: MainWindow):
        self.scanned_device_list = {}
        self.connected_device_list = {}
        self.main_window = main_window
        self.running_clients = {}
        self.graph_clients = {}

    def scan_devices(self, devices):
        # self.scanned_device_list.clear()
        device : BLEDevice
        for device in devices:
            self.scanned_device_list[device.address] = device

    def connected_device_add(self, client: BaseBleakClient):
        global client_name
        self.connected_device_list[client.address] = client
        self.main_window.col4_text_log.append(str(time.time()) + "\t" + client_name[client.address] + " Connected")

    def connected_device_remove(self, client:BaseBleakClient):
        global client_name
        if(client.address in self.running_clients.keys()):
            del self.running_clients[client.address]
        if(client.address in self.connected_device_list.keys()):
            del self.connected_device_list[client.address]
        self.main_window.col4_text_log.append(str(time.time()) + "\t" + client_name[client.address] + " Disconnected")

    def characteristicChanged(self, signal):
        global client_name
        client, uuid, value = signal
        self.main_window.col4_text_log.append("\t".join([str(time.time()), client_name[client.address], uuid, str(value)]))
        client_key = client.address + uuid
        if client_key in self.graph_clients.keys():
            self.graph_clients[client_key].appendData(value)

    def toggleNotification(self, client: BaseBleakClient, uuid, is_data_log_checked):
        if client.address not in self.running_clients.keys():
            self.running_clients[client.address] = {}
        if(uuid in self.running_clients[client.address].keys()):
            handler = self.running_clients[client.address][uuid]
            ret = handler.toggle_notification(is_data_log_checked)
        else:
            handler = notificationAgent(self, client, uuid, self.characteristicChanged)
            ret = handler.toggle_notification(is_data_log_checked)
            self.running_clients[client.address][uuid] = handler
        return ret

    def addGraphSession(self, client: BaseBleakClient, uuid, graph_infos):
        client_key = client.address + uuid
        if client_key in self.graph_clients.keys():
            self.graph_clients[client_key].canvas.glayout.close()
            del self.graph_clients[client_key]
        cur_graph_session = GraphSession(client, graph_infos, uuid == sibel_ppg_stream_uuid)
        self.graph_clients[client_key] = cur_graph_session

    def removeGraphSession(self, client: BaseBleakClient, uuid):
        client_key = client.address + uuid
        if client_key in self.graph_clients.keys():
            self.graph_clients[client_key].canvas.glayout.close()
            del self.graph_clients[client_key]

class GraphInfo:
    def __init__(self, channel_name, combo, bit_width, multiplier, length, is_signed, is_graph_enabled):
        self.channel_name = channel_name
        self.combo = combo
        self.bit_width = bit_width
        self.multiplier = multiplier
        self.length = length
        self.is_signed = is_signed
        self.is_graph_enabled = is_graph_enabled

sibel_stream_uuid_graph = {
    sibel_ppg_stream_uuid:[
        GraphInfo("Red", 1, 16, 1, 500, False, True),
        GraphInfo("IR", 1, 16, 1, 500, False, True)
    ],
    sibel_ecg_stream_uuid: [
        GraphInfo("ECG", 1, 12, 1, 500, True, True),
    ],
sibel_accl_stream_uuid: [
        GraphInfo("Z", 8, 16, 1 / 16384, 416, True, True),
        GraphInfo("X", 1, 16, 1 / 16384, 52, True, True),
        GraphInfo("Y", 1, 16, 1 / 16384, 52, True, True),
    ],
}

class Crc32:
    poly = 0xedb88320
    def __init__(self):
        self.table = [0] * 16 * 256
        for i in range(256):
            res = i
            for t in range(16):
                for k in range(8):
                    res = self.poly ^ (res >> 1) if res & 1 == 1 else (res >> 1)
                    self.table[t * 256 + i] = res
    def append(self, crc, input, offset, length):
        crc_local = 0xFFFFFFFF ^ crc
        while(length >= 16):
            a = self.table[(3 * 256) + input[offset + 12]] ^ self.table[(2 * 256) + input[offset + 13]] ^ self.table[(1 * 256) + input[offset + 14]] ^ self.table[(0 * 256) + input[offset + 15]]
            b = self.table[(7 * 256) + input[offset + 8]] ^ self.table[(6 * 256) + input[offset + 9]] ^ self.table[(5 * 256) + input[offset + 10]] ^ self.table[(4 * 256) + input[offset + 11]]
            c = self.table[(11 * 256) + input[offset + 4]] ^ self.table[(10 * 256) + input[offset + 5]] ^ self.table[(9 * 256) + input[offset + 6]] ^ self.table[(8 * 256) + input[offset + 7]]
            d = self.table[(15 * 256) + (crc_local ^ input[offset]) % 256] ^ self.table[(14 * 256) + ((crc_local >> 8) ^ input[offset + 1]) % 256] ^ self.table[(13 * 256)+ ((crc_local >> 16) ^ input[offset + 2]) % 256] ^ self.table[(12 * 256) + ((crc_local >> 24) ^ input[offset+3]) % 256]
            crc_local = d ^ c ^ b ^ a
            offset += 16
            length -= 16
        length -= 1
        while(length >= 0):
            crc_local = self.table[(crc_local ^ input[offset]) % 256] ^ crc_local >> 8
            offset += 1
            length -= 1
        return crc_local ^ 0xFFFFFFFF

def get_checkbox():
    chkBoxItem = QTableWidgetItem()
    chkBoxItem.setFlags(Qt.ItemIsUserCheckable | Qt.ItemIsEnabled)
    chkBoxItem.setCheckState(Qt.Unchecked)
    chkBoxItem.setTextAlignment(Qt.AlignCenter)
    return chkBoxItem

class GraphSession:
    def __init__(self, client:BaseBleakClient, graph_infos: List[GraphInfo], calc_vital):
        # QWidget.__init__(self, None)
        self.canvas = GraphCanvas(client, graph_infos)
        self.graph_infos = graph_infos

        self.calc_vital = calc_vital
        if(calc_vital):
            #HMON
            self.ppg_agent = PpgVitalSignAgent(128)
            self.time = 0

    def appendData(self, byte_array):
        byte_array = bytearray(byte_array)
        # byte_array.reverse()
        # print(np.array([x for x in byte_array]))
        new_barr = byte_array.copy()
        for i in range(len(byte_array)//2):
            new_barr[i*2] = byte_array[i*2+1]
            new_barr[i*2+1] = byte_array[i*2]
        bit_array = ''.join(format(byte, '08b') for byte in new_barr)
        bit_index = 0
        data_list = {}
        i = 0
        loop = True
        while (loop):
            if(self.graph_infos[i].channel_name not in data_list.keys()):
                data_list[self.graph_infos[i].channel_name] = []
            for j in range(self.graph_infos[i].combo):
                start_index = bit_index
                bit_index += self.graph_infos[i].bit_width
                stop_index = bit_index
                if(stop_index > len(bit_array)):
                    loop = False
                    break
                cur_bit = bit_array[start_index:stop_index]
                cur_int = BitArray(bin = cur_bit)#cur_bit[8:] + cur_bit[:8])
                if(self.graph_infos[i].is_signed):
                    cur_sint = cur_int.int
                else:
                    cur_sint = cur_int.uint
                data_list[self.graph_infos[i].channel_name].append(cur_sint * self.graph_infos[i].multiplier)
            i = (i + 1) % len(self.graph_infos)

        self.canvas.updateBuffer(data_list)
        if(self.calc_vital):
            #HMON
            dp = DataPacket()
            dp.is_data_valid = True
            dp.sampling_rate = 128
            dp.data = {}
            dp.state = {}
            dp.data[RawDataChannelType.PPG_T_IR] = data_list['IR']
            dp.data[RawDataChannelType.PPG_T_RED] = data_list['Red']
            dp.state[RawDataChannelType.PPG_DETACHED] = False
            dp.time = self.time + np.arange(len(data_list['IR'])) *  (1000 / 128)
            self.time += (1000 / 128) * len(data_list['IR'])
            self.ppg_agent.feed_data(dp)
            cur_spo2 = ti_calib[0] * self.ppg_agent.cur_spo2_ratio **2 + ti_calib[1] * self.ppg_agent.cur_spo2_ratio + ti_calib[2]
            cur_spo2_str= "{:.2f}".format(cur_spo2)
            cur_pi = self.ppg_agent.current_perfusion_index
            cur_pr = self.ppg_agent.current_pulse_rate
            title = f"SpO2: {cur_spo2_str}\t\tPI:{cur_pi}\t\tPR:{cur_pr}"
            self.canvas.axes[0].setTitle(title, size = "15pt")

class GraphCanvas(QWidget):
    def __init__(self, client:BaseBleakClient, graph_infos: List[GraphInfo]):
        global client_name
        self.buffers = []
        self.axes = []
        self.waveforms = []
        self.channel_map = {}

        self.layout = pg.LayoutWidget()
        self.glayout = pg.GraphicsLayoutWidget()
        self.layout.addWidget(self.glayout)
        subplots = 0
        for graph_info in graph_infos:
            if(graph_info.is_graph_enabled):
                cur_plot_item = pg.PlotItem(title= client_name[client.address] + "\t|\tCH : " + graph_info.channel_name)
                self.buffers.append(deque([0] * graph_info.length, maxlen=graph_info.length))
                self.axes.append(cur_plot_item)
                self.glayout.addItem(cur_plot_item, row=subplots, col= 0)
                self.waveforms.append(self.axes[-1].plot())
                self.channel_map[graph_info.channel_name] = subplots
                subplots += 1
        super(GraphCanvas, self).__init__()
        self.layout.show()

    def updateBuffer(self, data_list):
        for key in data_list.keys():
            if key in self.channel_map.keys():
                cur_buffer_ind = self.channel_map[key]
                for d in data_list[key]:
                    self.buffers[cur_buffer_ind].append(d)
                self.waveforms[cur_buffer_ind].setData((self.buffers[cur_buffer_ind]))

async def wakeup():
    return

class AsyncWakeup(QThread):
    signal = pyqtSignal(list)
    def __init__(self):
        super().__init__()

    def run(self):
        while True:
            time.sleep(0.3)
            self.signal.emit([])

async def writeData(client:BaseBleakClient, uuid, data:bytearray, response):
    try:
        await client.write_gatt_char(uuid, data, response)
        return True
    except:
        traceback.print_exc()
        return False

async def scanDevices():
    devices = await bleak.discover(1)
    return devices

async def connectDevices(device: BLEDevice, disconnect_callback, get_services = False):
    try:
        client = bleak.BleakClient(device.address, disconnected_callback= disconnect_callback)
        await client.connect()
        # async with bleak.BleakClient(device.address) as client:
        x = await client.is_connected()
        if(x and get_services):
            svcs = await client.get_services()
        return client, x
    except:
        return None, False

async def disconnectDevices(client: BaseBleakClient):
    try:
        await client.disconnect()
    except:
        pass

def send_data(hmon_session: HmonSession, client : BaseBleakClient, uuid, data: bytearray):
    global client_name
    loop = asyncio.get_event_loop()
    characteristic = client.services.get_characteristic(uuid)
    hmon_session.main_window.col4_text_log.append(" ".join([str(time.time()) + "\t" + client_name[client.address], "Sending Data to", uuid, ":", str(data)]))
    if("write-without-response" in characteristic.properties):
        response = False
    else:
        response = True
    ret = loop.run_until_complete(writeData(client, uuid, data, response))

def send_all(hmon_session: HmonSession, uuid, data:bytearray):
    global client_name
    order_map = {}
    name_order = []
    for client_address in hmon_session.connected_device_list.keys():
        client = hmon_session.connected_device_list[client_address]
        order_map[client_name[client.address]] = client_address
        name_order.append(client_name[client.address])
    name_order = sorted(name_order)
    for n in name_order:
        client = hmon_session.connected_device_list[order_map[n]]
        if(client.services.get_characteristic(uuid) != None):
            send_data(hmon_session, client, uuid, data)

class notificationAgent:
    signal = pyqtSignal(list)
    def __init__(self, hs:HmonSession, client, uuid, callback):
        self.hs = hs
        self.client : BaseBleakClient = client
        self.uuid = uuid
        self.callback = callback
        self.is_notifying = False
        self.is_filestream_opened = False
        self.is_shrd_stream = (uuid == sibel_shrd_stream_uuid)
        self.shrd_ind = 0
        self.session_crc32 = 0
        self.crc32 = Crc32()
        self.ParsingSHRD = False

    def __del__(self):
        if(self.is_filestream_opened):
            self.filestream.close()

    def open_filestream(self):
        global client_name
        self.is_filestream_opened = True
        cur_time = time.time()
        self.file_name = client_name[self.client.address]
        if(self.file_name + ".txt" in os.listdir(desktop_dir)):
            i = 1
            while(self.file_name + "-" + str(i) + ".txt" in os.listdir(desktop_dir)):
                i += 1
        if(not(self.is_shrd_stream)):
            self.filestream = open(os.path.join(desktop_dir, self.file_name + ".log"), mode='ab')
        with open(os.path.join(desktop_dir, self.file_name + ".txt"), mode = 'w') as f:
            lines = [
                f"File Name: {self.file_name}",
                f"Write Time: {str(cur_time)}",
                f"Client Name: {client_name[self.client.address]}",
                f"Client Address: {self.client.address}",
                f"Characteristic UUID: {str(self.uuid)}",
            ]
            f.write("\n".join(lines))

    def toggle_notification(self, is_data_log_checked):
        if (self.is_filestream_opened == False and is_data_log_checked):
            self.open_filestream()
        loop = asyncio.get_event_loop()
        return loop.run_until_complete(self._toggle_notification())

    async def _toggle_notification(self):
        try:
            if(not(self.is_notifying)):
                await self.client.start_notify(self.uuid, self.handle_notification)
                print("Notification Enabled")
                self.is_notifying = True
                return 1
            else:
                await self.client.stop_notify(self.uuid)
                print("Notification Disabled")
                self.is_notifying = False
                return 0

        except:
            traceback.print_exc()
            return -1

    # Handler for notification from 'Sibel SHRD Stream' characteristic
    def handle_notification(self, sender, data):
        if(self.is_shrd_stream):
            header = format(data[:1].hex())
            if(header == "0f"): # First page (Metadata)
                self.shrd_ind += 1
                if (self.is_filestream_opened):
                    self.filestream = open(os.path.join(desktop_dir, self.file_name + "-" + str(self.shrd_ind) + ".shrd"), mode='ab')
                epoch_time = data[4] ** 24 + data[3] ** 16 + data[2] ** 8 + data[1]
                device_index = data[9]
                redownload_flag = data[10]
                session_id = data[15]**32 + data[14] ** 24 + data[13] ** 16 + data[12] ** 8 + data[11]
                self.session_crc32 = 0
            elif(header == 'ff'): # Data pages
                if (self.is_filestream_opened):
                    self.filestream.write(data[1:])
                self.session_crc32 = self.crc32.append(self.session_crc32, bytearray(data), 1, len(data) - 1)
            elif(header == '11'): # CRC request
                send_data(self.hs, self.client, sibel_cmd_uuid, bytearray.fromhex("05") + self.session_crc32.to_bytes(4, 'little'))
                if (self.is_filestream_opened):
                    self.filestream.close()
            elif(header == '10'): # End of session/Download done
                if(self.ParsingSHRD):
                    # Parse SHRD
                    print("Parsing and plotting SHRD")
                    ParsedData = ShrdParser(os.path.join(desktop_dir, self.file_name + "-" + str(self.shrd_ind) + ".shrd"))
                    toPlot = ParsedData.parse_all()
                    number_types = len(toPlot)
                    for datatype in toPlot:
                        cur_ax = plt.subplot(number_types,1,toPlot.index(datatype)+1)
                        for ch in datatype.data:
                            cur_ax.plot(datatype.data[ch], label=ch.name)
                    plt.legend()
                    plt.show()
        else:
            if (self.is_filestream_opened):
                self.filestream.write(data)
        self.callback([self.client, self.uuid, data])

if __name__== '__main__':
    nest_asyncio.apply()
    app = QApplication(sys.argv)
    myGUI = MainWindow()
    app.exec_()
    sys.exit()