import sys
import datalab
from datalab import datalab_controller

def main():
    datalab_controller.main()

if __name__ == "__main__":
    sys.exit(main())
