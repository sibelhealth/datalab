import sys
import os

import PyQt5 as PyQt5
# from PyQt5.QtWidgets import *
# from PyQt5.QtCore import *
# from PyQt5.QtGui import *
# import PyQt5.QtWidgets as QtWidgets
# import pyqtgraph as pg

#from datalab.utilities.fetal_view_16_chn import *
from datalab.datalab_mainview import *
from datalab.datalab_model import *
from datalab.utilities.definitions import *
#from datalab.utilities.crc32 import *
from datalab.utilities.tasks import *

import bleak
from bleak import BleakClient, BleakScanner, discover
from bleak.backends.characteristic import BleakGATTCharacteristic

# from hmon import *
# from hmon.data.ShrdParser import ShrdParser
# from hmon.ppg.PpgVitalSignAgent import PpgVitalSignAgent
# from hmon.data.DataPacket import *
# from hmon.settings.enum import *

#import traceback
import asyncio
import time
#import threading
from inspect import signature

#import matplotlib.pyplot as plt
#from datetime import datetime
#from typing import List
#from collections import deque

class DataLab_Controller:
    def __init__(self):
        self.service = BLEService(self) # TODO Change the name of this property
        self.mainView = DataLab_MainView(self)
        self.ongoingTasks = []

    def scanDevices(self):
        ret = self.service.scanDevices()
        if ret:
            return self.service.ScannedDevices.values()
        else:
            msgBox = QMessageBox()
            msgBox.setText("Please run Datalab with admin privileges and make sure that the BT adapter is on")
            msgBox.exec()
            return []

    def connectDevice(self, address, name):
        client, success = self.service.connectDevice(address, name)
        if success:
            self.mainView.Tab_Widget.Tab1_Manual.refreshConnectedTree(self.service.ConnectedClients)
            self.mainView.Tab_Widget.Tab1_Manual.log(" - ".join(['{:.4f}'.format(time.time()), client.Name, "Connected"]))  
        else: 
            self.mainView.Tab_Widget.Tab1_Manual.log(" - ".join(['{:.4f}'.format(time.time()), name, "Could not connect"]))
    
    def disconnectClient(self, client):
        success = self.service.disconnectDevice(client)
        if success:
            self.mainView.Tab_Widget.Tab1_Manual.refreshConnectedTree(self.service.ConnectedClients)
            self.disconnectedClient(client)
        else: 
            self.mainView.Tab_Widget.Tab1_Manual.log(" - ".join(['{:.4f}'.format(time.time()), client.Name, "Could not disconnect"]))

    def refreshBits(self, client:BLEClient):
        registers = ['05']
        for reg in registers:
            data = "AA"+reg
            data = bytearray.fromhex(data)
            ret = self.writeToChar(client, sibel_cmd_uuid, data)
            self.mainView.Tab_Widget.tab2_Interactive.col2_scanned_device_table.setItem(registers.index(reg), 0, QTableWidgetItem(format(ret[:1].hex())))

    def transmitBits(self, client:BLEClient):
        for row in range(self.mainView.Tab_Widget.tab2_Interactive.col2_scanned_device_table.rowCount()):
            reg = self.mainView.Tab_Widget.tab2_Interactive.col2_scanned_device_table.verticalHeaderItem(row).text()[2:]
            val = self.mainView.Tab_Widget.tab2_Interactive.col2_scanned_device_table.item(row,0).text()
            data = "AB"+reg+val
            data = bytearray.fromhex(data)
            ret = self.writeToChar(client, sibel_cmd_uuid, data)
            self.mainView.Tab_Widget.tab2_Interactive.col2_scanned_device_table.setItem(row, 0, QTableWidgetItem(format(ret[:1].hex())))

    def writeRegister(self, client:BLEClient, afe:AFE, reg:bytearray, val:str):
        data = bytearray.fromhex('AC')
        data.extend(afe.ptr)
        data.extend(reg)
        data.extend(bytearray.fromhex(val))
        self.writeToChar(client, sibel_cmd_uuid, data)
        self.initDatalab()

    def writeChar(self, client : BLEClient, uuid, data: bytearray):
        ret = False
        characteristic = client.services.get_characteristic(uuid)
        ret = self.service.writeChar(client, characteristic, data)
        if ret:
            self.mainView.Tab_Widget.Tab1_Manual.log(" - ".join(['{:.4f}'.format(time.time()), client.Name, "W", uuid.split('-')[0][4:] + ': ' + data.hex()]))
        else:
            self.mainView.Tab_Widget.Tab1_Manual.log(" - ".join(['{:.4f}'.format(time.time()), client.Name, "W", uuid.split('-')[0][4:] + ': ' + 'Unsuccesful Write']))
        return ret
        
    def readChar(self, client : BLEClient, uuid):
        char = client.services.get_characteristic(uuid)
        ret = self.service.readChar(client, char)
        if ret:
            self.mainView.Tab_Widget.Tab1_Manual.log(" - ".join(['{:.4f}'.format(time.time()), client.Name, "R", uuid.split('-')[0][4:] + ': ' +  ret.hex()]))
        else:
            self.mainView.Tab_Widget.Tab1_Manual.log(" - ".join(['{:.4f}'.format(time.time()), client.Name, "R", uuid.split('-')[0][4:] + ': ' +  'Unsuccesful Read']))
        return ret

    def toggleNotification(self,client:BLEClient, uuid):
        char = client.services.get_characteristic(uuid)
        ret = self.service.toggleNotification(client, char)
        self.mainView.Tab_Widget.Tab1_Manual.col3_button_toggle_notification.setText("Enable Notification")
        if (char in client.notifying_characteristics.keys()):
            self.mainView.Tab_Widget.Tab1_Manual.col3_button_toggle_notification.setText("Disable Notification")
        return ret

    def getNotification(self, client:BLEClient, char:BleakGATTCharacteristic, data):
        #print('Notification from {} on {} '.format(client.Name, char.uuid))
        self.mainView.Tab_Widget.Tab1_Manual.log(" - ".join(['{:.4f}'.format(time.time()), client.Name, "N", char.uuid.split('-')[0][4:] + ': ' + 'len({})'.format(len(data)) +  data.hex()]))
        for task in [t for t in self.ongoingTasks if t.generator==char.uuid]:
            task.trigger(data)

    def send_all(self, uuid, data:bytearray):
        global client_name
        order_map = {}
        name_order = []
        for client_address in hmon_session.connected_clients.keys():
            client = hmon_session.connected_clients[client_address]
            order_map[client_name[client.address]] = client_address
            name_order.append(client_name[client.address])
        name_order = sorted(name_order)
        for n in name_order:
            client = hmon_session.connected_clients[order_map[n]]
            if(client.services.get_characteristic(uuid) != None):
                writeToChar(hmon_session, client, uuid, data)

    def addGraphSession(self, client, uuid,  graph_infos):
        self.service.addGraphSession(client, uuid, graph_infos)

    def stopSession(self, client, uuid):
        data = bytearray.fromhex("02")
        if(self.mainView.Tab_Widget.Tab1_Manual.col3_forall_checkbox.checkState() == Qt.Checked):
            self.send_all(uuid, data)
        else:
            self.writeToChar(client, uuid, data)

    def startSession(self, client, uuid):
        if(client.Name[0] == 'F'):
            data = bytearray.fromhex("01") + int(time.time()).to_bytes(4, 'little') + bytearray.fromhex("000000000000")
            if (self.mainView.Tab_Widget.Tab1_Manual.col3_forall_checkbox.checkState() == Qt.Checked):
                self.send_all(uuid, data)
            else:
                self.writeToChar(client, uuid, data)
        else:
            data = bytearray.fromhex("01") + int(time.time()).to_bytes(4, 'little')
            if (self.mainView.Tab_Widget.Tab1_Manual.col3_forall_checkbox.checkState() == Qt.Checked):
                self.send_all(uuid, data)
            else:
                self.writeToChar(client, uuid, data)

    def disconnectedClient(self, client):
        self.mainView.Tab_Widget.onTabChanged(0)
        self.mainView.Tab_Widget.Tab1_Manual.log(" - ".join(['{:.4f}'.format(time.time()), client.Name, "Disconnected"]))    
        self.removeOngoingTask()

    def getConnectedClients(self):
        return self.service.ConnectedClients

    # def executeTaskAsync(self, tasks):
    #     self.new_thread = QThread()
    #     self.new_worker = Worker(tasks)
    #     self.new_worker.moveToThread(self.new_thread)
    #     self.new_thread.started.connect(self.new_worker.run)
    #     self.new_worker.finished.connect(self.new_thread.quit)
    #     self.new_worker.finished.connect(self.new_worker.deleteLater)
    #     self.new_thread.finished.connect(self.new_thread.deleteLater)
    #     self.new_worker.execute_task_signal.connect(self.executeTasks)
    #     self.new_thread.start()

    def removeOngoingTask(self):
        for ongoing_task in self.ongoingTasks:
            if not ongoing_task.ongoing:
                self.ongoingTasks.remove(ongoing_task)


    def executeTasks(self, tasks:list):
        for task in tasks:
            for step in task.sequence:
                op = step[0]
                uuid = step[1]
                cmd = step[2]
                ret = None
                if op == 'r':
                    ret = self.readChar(task.client, uuid)
                if op == 'w':
                    ret = self.writeChar(task.client, uuid, cmd)
                if op == 'n':
                    ret = self.toggleNotification(task.client,uuid)
                if op == 'p':
                    ret = False
                    ret = task.setPlot(self.mainView.Tab_Widget.tab2_Interactive.createNewPlotItem(task))
                if op == 't':
                    ret = True
                if op == 'h':
                    ret = True
            if task.ongoing:
                if task not in self.ongoingTasks:
                    self.ongoingTasks.append(task)
            self.removeOngoingTask()
            task.result = ret
            self.mainView.Tab_Widget.tab2_Interactive.reportTask(task)

    def createBatteryTask(self,client):
        task = BatteryLevelTask(client)
        return task

    def createDLInfoTask(self, client):
        task = DLInfoTask(client)   
        return task

    def createStartSessionTask(self,client):
        task = StartSessionTask(client)
        return task

    def createStopSessionTask(self, client):
        task = StopSessionTask(client)   
        return task

    def createToggleNotificationTask(self, client, char):
        task = ToggleNotificationTask(client, char.uuid)
        return task

    def createPlotNotificationTask(self, client, char):
        task = PlotNotificationTask(client, char.uuid)
        return task

    def createReadCharacteristicTask(self, client, char):
        task = ReadTask(client, char.uuid)
        return task

    def createWriteCharacteristicTask(self, client, char, cmd):
        task = WriteTask(client, char.uuid, cmd)
        return task

    def createDnldSessionsTask(self, client, file_name):
        task = DownloadSessionTask(client, file_name)
        return task

    def getAvailableTasks(self):
        return baseTask.subclasses

    def executeTest(self, client, task_list):
        tasks = []
        for task_name in task_list:
            if 'Plot' in task_name:
                name = task_name.split(' on uuid: ')[0]
                char = task_name.split(' on uuid: ')[1]
                params = [client, char] 
            elif 'Toggle' in task_name:
                name = task_name.split(' on uuid: ')[0]
                char = task_name.split(' on uuid: ')[1]
                params = [client, char]
            elif 'Timer' in task_name:
                name = task_name.split(' for ')[0]
                params = task_name.split(' for ')[1]
                params = [client, int(params.split(' secs')[0])]
            elif 'Download' in task_name:
                name = task_name.split(' on file: ')[0]
                file_name = task_name.split(' on file: ')[1]
                params = [client, file_name]
            elif 'Write' in task_name:
                name = task_name.split(' on')[0]
                char = task_name.split(': ')[1].split(' with')[0]
                cmd = bytearray.fromhex(task_name.split(' with cmd: ')[1])
                params = [client, char, cmd]
            elif 'Read' in task_name:
                name = task_name.split(' on')[0]
                char = task_name.split(': ')[1]
                params = [client, char]
            else:
                name = task_name
                params = [client]
            try:
                tasks.append(baseTask.create(name, params))
            except:
                msgBox = QMessageBox()
                msgBox.setText("Task {} could not be parsed into a task".format(task_name))
                msgBox.exec()
                return
        self.executeTestAsync(tasks)
             
    def executeTestAsync(self, tasks):
        self.thread = QThread()
        self.worker = Worker(tasks)
        self.worker.moveToThread(self.thread)

        self.thread.started.connect(self.worker.run)

        self.worker.finished.connect(lambda: print('worker finished signal'))
        self.worker.finished.connect(self.thread.quit)
        self.worker.finished.connect(self.worker.deleteLater)
        self.thread.finished.connect(self.thread.deleteLater)
        self.thread.finished.connect(self.mainView.Tab_Widget.tab3_Designer.finishExecution)

        self.worker.execute_task_signal.connect(lambda tasks: print('worker execute signal on task ' + tasks[0].name))
        self.worker.execute_task_signal.connect(self.executeTasks)
        self.worker.new_line.connect(self.mainView.Tab_Widget.tab3_Designer.updateExecution)
        self.thread.start()

    def getTaskArgs(self, task_str):
        sig = signature(baseTask.subclasses[task_str])
        return sig

class Worker(QObject):
    execute_task_signal = pyqtSignal(list)
    new_line = pyqtSignal(int)
    finished = pyqtSignal()

    def __init__(self, tasks):
        QThread.__init__(self)
        self.tasks = tasks


    def run(self):
        i = 0
        taskidx = 0
        while i < len(self.tasks):
            self.new_line.emit(taskidx)
            task = self.tasks[i]
            # for task in self.tasks:
            
            self.executeTask(task)
            while not task.done:
                time.sleep(0.01)
            if task.continue_with:
                taskidx -= 1
                new_task = task.continue_with
                self.tasks.insert(i+1, new_task)
                task.ongoing = False
            i += 1
            taskidx += 1
        self.finished.emit()

    def executeTask(self,task):
        if isinstance(task, TimerTask):
            self.execute_task_signal.emit([task])
            time.sleep(task.time)
        else:
            self.execute_task_signal.emit([task])

def StartApp():
    os.environ["QT_ENABLE_HIGHDPI_SCALING"] = "1"
    app = QApplication(sys.argv)
    controller = DataLab_Controller()
    app.exec_()
    return controller

def main():
    #nest_asyncio.apply()
    controller = StartApp()
    sys.exit()

if __name__== '__main__':
    main()
