import sys
import os

#from datalab_controller import DataLab_MainView_Controller

from PyQt5.QtCore import Qt
from PyQt5.QtWidgets import *
from PyQt5.QtWidgets import QSizePolicy, QDialog, QDialogButtonBox, QLabel, QMainWindow, QWidget, QApplication, QFileDialog
from PyQt5.QtCore import QThread, pyqtSignal
from PyQt5.QtGui import *

from pyqtgraph import GraphicsLayoutWidget
import pyqtgraph as pg

# pg.setConfigOption('background', 'w')
# pg.setConfigOption('foreground', 'k')

import time

from datalab.utilities.definitions import *

#from typing import List
#import asyncio


class graphParamsDlg(QDialog):
    def __init__(self, parent=None):
        super().__init__(parent=parent)

        self.setWindowTitle("Set Notification Parsing Parameters")

        QBtn = QDialogButtonBox.Ok

        self.buttonBox = QDialogButtonBox(QBtn)
        self.buttonBox.accepted.connect(self.accept)

        self.table = QTableWidget()  
        self.table.setColumnCount(6)
        self.table.setRowCount(1)
        self.table.setHorizontalHeaderLabels(['ID', 'No of Samples', 'Resolution', 'Signed?', 'Lil Endian>','Sampling Freq'])
        self.table.setFocusPolicy(Qt.NoFocus)

        self.bot_range = QLineEdit()
        self.top_range = QLineEdit()

        self.layout = QVBoxLayout()

        self.layout.addWidget(QLabel('First Byte'))
        self.layout.addWidget(self.bot_range)
        self.layout.addWidget(QLabel('Last Byte'))
        self.layout.addWidget(self.top_range)
        self.layout.addWidget(self.table)
        self.layout.addWidget(self.buttonBox)

        self.add_row_btn = QPushButton('Add Row')
        self.add_row_btn.clicked.connect(self.addRow)
        self.remove_row_btn = QPushButton('Remove Row')
        self.remove_row_btn.clicked.connect(self.removeRow)

        self.buttonlayout = QHBoxLayout()
        self.buttonlayout.addWidget(self.add_row_btn)
        self.buttonlayout.addWidget(self.remove_row_btn)

        self.layout.addLayout(self.buttonlayout)        
        
        self.setLayout(self.layout)

        self.table.resizeRowsToContents()
        self.table.resizeColumnsToContents()
        self.adjustSize()

        self.resize(1000,500)


    def addRow(self):
        idx = self.table.rowCount()
        self.table.insertRow(idx)
        self.table.setItem(idx, 0, QTableWidgetItem(''))
        self.table.setItem(idx, 1, QTableWidgetItem(''))
        self.table.setItem(idx, 2, QTableWidgetItem(''))
        self.table.setItem(idx, 3, self.getCheckbox())
        self.table.setItem(idx, 4, self.getCheckbox())
        self.table.setItem(idx, 5, QTableWidgetItem(''))

    def removeRow(self):
        row = self.table.currentRow()
        self.table.removeRow(row)

    def getCheckbox(self):
        chkBoxItem = QTableWidgetItem()
        chkBoxItem.setFlags(Qt.ItemIsUserCheckable | Qt.ItemIsEnabled)
        chkBoxItem.setCheckState(Qt.Unchecked)
        chkBoxItem.setTextAlignment(Qt.AlignCenter)
        return chkBoxItem

class selectCharacteristicDlg(QDialog):
    def __init__(self, parent=None):
        super().__init__(parent=parent)

        self.setWindowTitle("Select Characteristic")

        QBtn = QDialogButtonBox.Ok

        self.buttonBox = QDialogButtonBox(QBtn)
        self.buttonBox.accepted.connect(self.accept)

        # Form
        self.layout = QVBoxLayout()

        self.layout.addWidget(QLabel('Type the characteristic UUID for this Task:'))
        self.edit_characteristic = QLineEdit()
        self.layout.addWidget(self.edit_characteristic)

        self.layout.addWidget(QLabel('Or select a characteristic from this list:'))

        self.table = QTableWidget()
        self.table.setRowCount(0)
        self.table.setColumnCount(2)
        self.table.setHorizontalHeaderLabels(['NAME','UUID'])
        self.table.setEditTriggers(QTableWidget.NoEditTriggers)
        self.table.setSelectionBehavior(QAbstractItemView.SelectRows)
        self.table.setSelectionMode(QAbstractItemView.SingleSelection)
        self.table.itemClicked.connect(self.onCharItemClick)
        
        for service in sibel_uuid:
            row = self.table.rowCount()
            self.table.insertRow(row)
            self.table.setItem(row,0,QTableWidgetItem(sibel_uuid[service]))
            self.table.setItem(row,1,QTableWidgetItem(service))
        
        self.table.resizeRowsToContents()
        self.table.resizeColumnsToContents()
        self.layout.addWidget(self.table)

        self.layout.addWidget(self.buttonBox)
        
        self.setLayout(self.layout)
        self.adjustSize()

        self.resize(500,800)

    def onCharItemClick(self, it):
        row = self.table.currentRow()
        self.edit_characteristic.clear()
        self.edit_characteristic.setText(self.table.item(row,1).text())

class DataLab_MainView(QMainWindow): 
    def __init__(self, caller): 
        super().__init__() 

        global controller
        controller = caller

        self.setWindowTitle("DataLab Bench")
  
        self.Tab_Widget = TabsContainer(self) 
        self.setCentralWidget(self.Tab_Widget) 
        self.showMaximized()
        self.show()

        #self.clearGraphTable()
        self.async_wakeupper = AsyncWakeup()
        self.async_wakeupper.signal.connect(self.wakeup)
        self.async_wakeupper.start()
        self.scan_device = False
        self.scanned_device_list = []
        self.connected_device_list = []
        self.running_clients = []

        return

    def wakeup(self, arg):
        # print(arg)
        loop = asyncio.get_event_loop()
        if(not loop.is_running()):
            loop.run_until_complete(wakeup())
        pass

class TabsContainer(QWidget):
    def __init__(self, parent): 
        super().__init__() 
        self.layout = QVBoxLayout(self) 
  
        # Initialize tab screen 
        self.tabs = QTabWidget() 
        self.Tab1_Manual = TabWidget_ManualUse(parent) 
        self.tab2_Interactive = TabWidget_Interactive(parent)
        self.tab3_Designer = TabWidget_Designer(parent)
        self.setSizePolicy(QSizePolicy.Maximum, QSizePolicy.Maximum)
  
        # Add tabs 
        self.tabs.addTab(self.Tab1_Manual, "Manual Use") 
        self.tabs.addTab(self.tab2_Interactive, "Interactive") 
        self.tabs.addTab(self.tab3_Designer, "Test Designer") 
  
        # Add tabs to widget 
        self.layout.addWidget(self.tabs)
        self.setLayout(self.layout) 

        self.tabs.currentChanged.connect(self.onTabChanged)

    def onTabChanged(self, idx):
        self.tab2_Interactive.hideButtons()
        self.tab2_Interactive.refreshInteractiveList()
        self.tab3_Designer.resetView()
        self.Tab1_Manual.resetView()

class TabWidget_ManualUse(QWidget):
    def __init__(self,_MainModel:DataLab_MainView):
        super().__init__()  

        self.ScannedDevices = []
        
        self.LAYOUT_MAIN = QHBoxLayout()
        self.setLayout(self.LAYOUT_MAIN)

        #self.LAYOUT_COL1_INIT = QVBoxLayout()
        #self.FRAME_COL1_INIT = QFrame(self)
        #self.FRAME_COL1_INIT.setLayout(self.LAYOUT_COL1_INIT)
        #self.col1_flo = QFormLayout()
        #self.LAYOUT_COL1_INIT.addLayout(self.col1_flo)

        self.LAYOUT_COL1_SCANNED = QVBoxLayout()
        self.FRAME_COL1_DEV = QFrame(self)
        self.FRAME_COL1_DEV.setLayout(self.LAYOUT_COL1_SCANNED)
        self.col2_scanned_device_table = QTableWidget()
        self.col2_scanned_device_table.setColumnCount(3)
        self.col2_scanned_device_table.setRowCount(50)
        self.col2_scanned_device_table.setHorizontalHeaderLabels(["NAME", "RSSI", "ADDRESS"])
        self.col2_scanned_device_table.setEditTriggers(QTableWidget.NoEditTriggers)
        self.col2_scanned_device_table.setSelectionBehavior(QAbstractItemView.SelectRows)
        self.col2_scanned_device_table.setSelectionMode(QAbstractItemView.SingleSelection)
        self.LAYOUT_COL1_SCANNED.addWidget(self.col2_scanned_device_table)
        self.col2_device_search = QLineEdit()
        self.col2_device_search.setPlaceholderText("Filter Devices")
        
        self.col2_device_search.textChanged.connect(self.onDeviceSearcTextChanged)
        self.LAYOUT_COL1_SCANNED.addWidget(self.col2_device_search)
        self.col2_button_hbox = QHBoxLayout()
        self.LAYOUT_COL1_SCANNED.addLayout(self.col2_button_hbox)
        self.col2_button_scan = QPushButton("Scan (Ctrl+R)")
        self.col2_button_scan.setShortcut(QKeySequence("Ctrl+R"))
        self.col2_button_scan.clicked.connect(self.onScanDeviceBtnClick)
        self.col2_button_connect = QPushButton("Connect Ctrl+A")
        self.col2_button_connect.setShortcut(QKeySequence("Ctrl+A"))
        self.col2_button_connect.clicked.connect(self.onConnectDeviceBtnClick)
        self.col2_scanned_device_table.doubleClicked.connect(self.onConnectDeviceBtnClick)
        self.col2_button_disconnect = QPushButton("Disconnect Ctrl+D")
        self.col2_button_disconnect.setShortcut(QKeySequence("Ctrl+D"))
        self.col2_button_disconnect.clicked.connect(self.onDisconnectDeviceBtnClick)
        self.col2_button_hbox.addWidget(self.col2_button_scan)
        self.col2_button_hbox.addWidget(self.col2_button_connect)
        self.col2_button_hbox.addWidget(self.col2_button_disconnect)

        self.LAYOUT_COL3_UUID = QVBoxLayout()
        self.FRAME_COL3_UUID = QFrame(self)
        self.FRAME_COL3_UUID.setLayout(self.LAYOUT_COL3_UUID)
        self.col3_connected_tree = QTreeWidget()
        self.col3_connected_tree.header().setStretchLastSection(False)
        self.col3_connected_tree.header().setSectionResizeMode(QHeaderView.ResizeToContents)
        self.col3_connected_tree.itemClicked.connect(self.onCharacteristicItemClick)
        self.col3_connected_tree.setColumnCount(3)
        self.col3_connected_tree.setHeaderLabels(["NAME", "SVS", "CHAR", "PROPERTIES", "DESCRIPTION"])
        self.col3_connected_tree.setSelectionBehavior(QAbstractItemView.SelectRows)
        self.col3_connected_tree.setSelectionMode(QAbstractItemView.SingleSelection)
        self.LAYOUT_COL3_UUID.addWidget(self.col3_connected_tree)

        # self.col3_graph_table = QTableWidget(QSizePolicy.Preferred, QSizePolicy.Preferred)
        # self.col3_graph_table.setColumnCount(7)
        # len_channel = 16
        # self.col3_graph_table.setRowCount(len_channel)
        # self.col3_graph_table.setHorizontalHeaderLabels(["Channel Name", "Combo", "Bit Width", "Multiplier", "Length", "Signed", "Enable Graph"])
        # self.col3_graph_table.setFocusPolicy(Qt.NoFocus)
        # self.col3_graph_table.resizeColumnsToContents()
        # self.col3_graph_table_btn =  QPushButton("Start Graph")
        # self.col3_graph_table_btn.clicked.connect(self.startGraphBtnAction)
        # self.col3_graph_table_clear_btn = QPushButton("Close Graph")
        # self.col3_graph_table_clear_btn.clicked.connect(self.removeGraphBtnAction)
        # self.LAYOUT_COL3_UUID.addWidget(self.col3_graph_table)
        # self.LAYOUT_COL3_UUID.addWidget(self.col3_graph_table_btn)

        self.col3_notification_hbox = QHBoxLayout()
        self.LAYOUT_COL3_UUID.addLayout(self.col3_notification_hbox)
        self.col3_button_toggle_notification = QPushButton("Enable Notifications")
        self.col3_button_toggle_notification.clicked.connect(self.onToggleNotificationBtnClick)
        self.col3_notification_hbox.addWidget(self.col3_button_toggle_notification)
        
        self.col3_button_hbox = QHBoxLayout()
        # self.LAYOUT_COL3_UUID.addLayout(self.col3_button_hbox)
        # #self.col3_forall_checkbox = QCheckBox("For All")
        # #self.col3_button_hbox.addWidget(self.col3_forall_checkbox)
        # self.col3_hex_input = QLineEdit()
        # validator = QRegExpValidator(QRegExp("[0-9A-Fa-f_/ -]{0,}"))
        # self.col3_hex_input.setValidator(validator)
        # self.col3_button_hbox.addWidget(self.col3_hex_input)
        
        self.col3_button_read = QPushButton("Read")
        self.col3_button_read.clicked.connect(self.onReadBtnClick)
        self.col3_button_hbox.addWidget(self.col3_button_read)

        self.col3_button_write = QPushButton("Write")
        self.col3_button_write.setShortcut(QKeySequence("Return"))
        self.col3_button_write.clicked.connect(self.onWriteBtnClick)
        self.col3_button_hbox.addWidget(self.col3_button_write)

        self.LAYOUT_COL4_GATT = QVBoxLayout()
        self.FRAME_COL4_GATT = QFrame(self)
        self.FRAME_COL4_GATT.setLayout(self.LAYOUT_COL4_GATT)
        self.col4_text_log = QTextEdit()
        # self.col4_text_log.NoWrap(True)
        self.col4_text_log.setLineWrapMode(False)
        font = self.col4_text_log.currentFont()
        font.setStyleHint(QFont.TypeWriter)
        font.setFamily('Courier')
        self.col4_text_log.setFont(font)
        self.LAYOUT_COL4_GATT.addWidget(self.col4_text_log)

        #self.LAYOUT_MAIN.addWidget(self.FRAME_COL1_INIT)
        self.LAYOUT_MAIN.addWidget(self.FRAME_COL1_DEV,1)
        self.LAYOUT_MAIN.addWidget(self.FRAME_COL3_UUID,2)
        self.LAYOUT_MAIN.addWidget(self.FRAME_COL4_GATT,2)

        self.hideButtons()  
        return

    # TODO: CHANGE THIS TO HIDE ALL CHARCTERISTIC-DEPENDENT BUTTONs AUTOMATICALLY
    def hideButtons(self): 
        self.col3_button_toggle_notification.hide()
        self.col3_button_write.hide()
        #self.col3_hex_input.hide()
        self.col3_button_read.hide()
        #self.col3_graph_table.hide()
        #self.col3_graph_table_btn.hide()

    def refreshConnectedTree(self,ConnectedClients):
        self.col3_connected_tree.clear()
        for client in ConnectedClients:
            client_item = QTreeWidgetItem(self.col3_connected_tree)
            client_item.setText(0, client.Name)
            client_item.setExpanded(True)
            client_item.setData(10, 0, client)
            myFont = QFont()
            myFont.setBold(True)
            client_item.setFont(0, myFont)
            for cs in client.services:
                service = QTreeWidgetItem(client_item)
                service.setText(1, cs.uuid.split('-')[0][4:])
                service.setExpanded(True)
                service.setData(10, 0, client)
                for cc in cs.characteristics:
                    characteristic = QTreeWidgetItem(service)
                    characteristic.setData(11,0,cc)
                    characteristic.setText(2, cc.uuid.split('-')[0][4:])
                    if(cc.uuid in sibel_uuid.keys()):
                        characteristic.setText(4, sibel_uuid[cc.uuid])
                    else:
                        characteristic.setText(4, cc.description)
                    characteristic.setText(3, str(cc.properties))
                    characteristic.setExpanded(True)
                    characteristic.setData(10, 0, client)
                    if (cc in client.notifying_characteristics.keys()):
                        myFont = QFont()
                        myFont.setBold(True)
                        characteristic.setFont(2, myFont)
                        characteristic.setFont(3, myFont)
                        characteristic.setFont(4, myFont)
                        characteristic.setForeground(2,QBrush(QColor(40,200,100)))
                        characteristic.setForeground(3,QBrush(QColor(40,200,100)))
                        characteristic.setForeground(4,QBrush(QColor(40,200,100)))

    def clearGraphTable(self):
        for i in range(self.col3_graph_table.rowCount()):
            for j in range(self.col3_graph_table.columnCount()):
                self.col3_graph_table.setItem(i, j, QTableWidgetItem())
            self.col3_graph_table.setItem(i, 5, get_checkbox())
            self.col3_graph_table.setItem(i, 6, get_checkbox())

    def onScanDeviceBtnClick(self):
        self.ScannedDevices = controller.scanDevices()
        self.populateDeviceTable()

    def updateScannedTable(self, scanned_devs):
        self.ScannedDevices = scanned_devs
        self.populateDeviceTable()
        
    # TODO remove the global ScannedDevices and replace with the locl table contents
    def onDeviceSearcTextChanged(self):
        self.populateDeviceTable()

    def populateDeviceTable(self):
        self.col2_scanned_device_table.clearContents()
        i = 0
        for device in self.ScannedDevices:
            if device:
                if (self.col2_device_search.text() in device.name and device.name != "Unknown"):
                    self.col2_scanned_device_table.setItem(i, 0, QTableWidgetItem(device.name))
                    self.col2_scanned_device_table.setItem(i, 1, QTableWidgetItem(str(device.rssi)))
                    self.col2_scanned_device_table.setItem(i, 2, QTableWidgetItem(device.address))
                    i += 1
        self.col2_scanned_device_table.resizeColumnsToContents()

    def onConnectDeviceBtnClick(self):
        row = self.col2_scanned_device_table.currentRow()
        if(self.col2_scanned_device_table.item(row,2) is not None):
            Address = self.col2_scanned_device_table.item(row,2).text()
            Name = self.col2_scanned_device_table.item(row,0).text()
            controller.connectDevice(Address, Name)
        
    def log(self,what:str):
        self.col4_text_log.append(what)
        # TODO implement log saving functionality to a file

    def onDisconnectDeviceBtnClick(self):
        col = self.col3_connected_tree.currentColumn()
        if(col > -1):
            it: QTreeWidgetItem
            it = self.col3_connected_tree.currentItem()
            client: BLEClient = it.data(10, 0)
            controller.disconnectClient(client)

    def onCharacteristicItemClick(self, it, col):
        """Called when the user clicks on a characteristic of a connected device

        Parses the buttons available to interact with the characteristic
        """
        client: BLEClient = it.data(10, 0)
        char = it.data(11,0)
        self.hideButtons()
        if (isinstance(char,bleak.backends.characteristic.BleakGATTCharacteristic)):
            if("notify" in char.properties):
                self.col3_button_toggle_notification.setText("Enable Notification")
                myFont = QFont()
                myFont.setBold(False)
                self.col3_connected_tree.currentItem().setFont(2, myFont)
                self.col3_connected_tree.currentItem().setFont(3, myFont)
                self.col3_connected_tree.currentItem().setFont(4, myFont)
                self.col3_connected_tree.currentItem().setForeground(2,QBrush(QColor(0,0,0)))
                self.col3_connected_tree.currentItem().setForeground(3,QBrush(QColor(0,0,0)))
                self.col3_connected_tree.currentItem().setForeground(4,QBrush(QColor(0,0,0)))
                self.col3_button_toggle_notification.show()
                if (char in client.notifying_characteristics.keys()):
                    self.col3_button_toggle_notification.setText("Disable Notification")
                    myFont = QFont()
                    myFont.setBold(True)
                    self.col3_connected_tree.currentItem().setFont(2, myFont)
                    self.col3_connected_tree.currentItem().setFont(3, myFont)
                    self.col3_connected_tree.currentItem().setFont(4, myFont)
                    self.col3_connected_tree.currentItem().setForeground(2,QBrush(QColor(40,200,100)))
                    self.col3_connected_tree.currentItem().setForeground(3,QBrush(QColor(40,200,100)))
                    self.col3_connected_tree.currentItem().setForeground(4,QBrush(QColor(40,200,100)))
            if("write" in char.properties or "write-without-response" in char.properties):
                self.col3_button_write.show()
                self.col3_hex_input.show()
                self.col3_hex_input.setFocus()
            if("read" in char.properties):
                self.col3_button_read.show()
   
    def onToggleNotificationBtnClick(self):
        it: QTreeWidgetItem
        it = self.col3_connected_tree.currentItem()
        client: BLEClient = it.data(10, 0)
        uuid = it.data(11,0).uuid
        if(uuid != ''):
            controller.toggleNotification(client,uuid)
            self.onCharacteristicItemClick(it,0) # Refresh visuals
            
    def onWriteBtnClick(self):
        col = self.col3_connected_tree.currentColumn()
        it: QTreeWidgetItem
        it = self.col3_connected_tree.currentItem()
        client: BLEClient = it.data(10, 0)
        uuid = it.data(11,0).uuid
        data = self.col3_hex_input.text()
        data = data.replace("_", "")
        data = data.replace("/", "")
        data = data.replace("-", "")
        data = data.replace(" ", "")
        data = data.replace("0x", "")
        if(uuid != ''):
            data = bytearray.fromhex(data)
            ret = controller.writeChar(client, uuid, data)

    def onReadBtnClick(self):
        col = self.col3_connected_tree.currentColumn()
        it: QTreeWidgetItem
        it = self.col3_connected_tree.currentItem()
        client: BLEClient = it.data(10, 0)
        uuid = it.data(11,0).uuid
        if(uuid != ''):
            ret = controller.readChar(client, uuid)

    def onStartBtnClick(self):
        col = self.col3_connected_tree.currentColumn()
        it: QTreeWidgetItem
        it = self.col3_connected_tree.currentItem()
        client: BaseBleakClient = it.data(10, 0)
        uuid = it.text(2)
        if(uuid == sibel_cmd_uuid):
            controller.startSession(client,uuid)
            
    def onStopBtnClick(self):
        col = self.col3_connected_tree.currentColumn()
        it: QTreeWidgetItem
        it = self.col3_connected_tree.currentItem()
        client: BLEClient = it.data(10, 0)
        uuid = it.text(2)
        if(uuid == sibel_cmd_uuid):
            controller.stopSession(client, uuid)
        # Pop up to download shrd file
        msg = QMessageBox()
        msg.setWindowTitle("File Download")
        msg.setText("Upload shrd?")
        msg.setStandardButtons(QMessageBox.Yes | QMessageBox.No)
        msg.setDefaultButton(QMessageBox.No)
        msg.buttonClicked.connect(self.onFileDownloadPopupClick)
        x = msg.exec_()  # this will show our messagebox

    def resetView(self):
        clients = controller.getConnectedClients()
        self.refreshConnectedTree(clients)

class TabWidget_Interactive(QWidget):
    def __init__(self,_MainModel:DataLab_MainView):
        super().__init__()  
        self.LAYOUT_MAIN = QHBoxLayout()
        self.setLayout(self.LAYOUT_MAIN)

        self.LAYOUT_COL1_COMPATIBLE = QVBoxLayout()
        self.FRAME_COL1_DEV = QFrame(self)
        self.FRAME_COL1_DEV.setLayout(self.LAYOUT_COL1_COMPATIBLE)

        self.layout_col2 = QVBoxLayout()
        self.frame_col2 = QFrame(self)
        self.frame_col2.setLayout(self.layout_col2)

        self.col1_compatible_device_table = QTableWidget()
        self.col1_compatible_device_table.setColumnCount(1)
        self.col1_compatible_device_table.setRowCount(0)
        self.col1_compatible_device_table.setHorizontalHeaderLabels(["CLIENTS"])
        self.col1_compatible_device_table.setEditTriggers(QTableWidget.NoEditTriggers)
        self.col1_compatible_device_table.setSelectionBehavior(QAbstractItemView.SelectRows)
        self.col1_compatible_device_table.setSelectionMode(QAbstractItemView.SingleSelection)
        self.col1_compatible_device_table.itemClicked.connect(self.onCompatibleClientClick)
        self.LAYOUT_COL1_COMPATIBLE.addWidget(self.col1_compatible_device_table)

        # Design widget for interaction with characteristics   
        self.groupbox_col2_interact = QGroupBox()
        self.layout_col2_interact = QGridLayout()
        self.groupbox_col2_interact.setLayout(self.layout_col2_interact)


        # Defining col 2 interaction widgets-------------------------------------
        self.interact_button_battery = QPushButton("Get Battery Level")
        self.interact_button_battery.clicked.connect(self.onGetBatteryBtnClick)
        self.layout_col2_interact.addWidget(self.interact_button_battery, 0, 0)

        self.interact_button_DLInfo = QPushButton("Get Data Logging Info")
        self.interact_button_DLInfo.clicked.connect(self.onGetDLInfoBtnClick)
        self.layout_col2_interact.addWidget(self.interact_button_DLInfo, 1, 0)
        
        self.interact_button_start_session = QPushButton("Start Session")
        self.interact_button_start_session.clicked.connect(self.onStartSessionBtnClick)
        self.layout_col2_interact.addWidget(self.interact_button_start_session, 2, 0)

        self.interact_button_stop_session = QPushButton("Stop Session")
        self.interact_button_stop_session.clicked.connect(self.onStopSessionBtnClick)
        self.layout_col2_interact.addWidget(self.interact_button_stop_session, 3, 0)

        self.interact_button_dnld_session = QPushButton("Download Sessions")
        self.interact_button_dnld_session.clicked.connect(self.onDnldSessionBtnClick)
        self.layout_col2_interact.addWidget(self.interact_button_dnld_session, 4, 0)

        # -----------------------------------------------------------------------------

        # Defining notifying characteristics table---------------------------------
        self.interact_char_table = QTableWidget()
        self.interact_char_table.setColumnCount(6)
        self.interact_char_table.setRowCount(0)
        self.interact_char_table.setHorizontalHeaderLabels(['NAME','UUID','NOTIFICATION','PLOT','READ','WRITE'])
        #self.interact_char_table.setEditTriggers(QTableWidget.NoEditTriggers)
        self.interact_char_table.setSelectionBehavior(QAbstractItemView.SelectRows)
        self.interact_char_table.setSelectionMode(QAbstractItemView.SingleSelection)
        self.layout_col2_interact.addWidget(self.interact_char_table, 0, 1, 5, 1)

        #self.interact_table_reg = QTableWidget()
        #self.layout_col2_interact.addWidget(self.interact_table_reg, 0,1,-1,1)

        self.layout_col2.addWidget(self.groupbox_col2_interact, 2) 
        self.layout_col2.addWidget(QLabel('Results:'))
        self.interact_tsk_response = QTextEdit()
        self.layout_col2.addWidget(self.interact_tsk_response, 1)


        # Defining col 3 (notification plotting) widgets
        self.groupbox_col3_graphs = QGroupBox()
        self.groupbox_col3_graphs.setTitle('Graphing Notifications')
        self.layout_col3_graphs = QVBoxLayout()
        self.groupbox_col3_graphs.setLayout(self.layout_col3_graphs)

        # self.graphic_widget_col3 = GraphicsLayoutWidget(show=True, border=(100,100,100))
        # self.layout_col3_graphs.addWidget(self.graphic_widget_col3)


        self.LAYOUT_MAIN.addWidget(self.FRAME_COL1_DEV, 1)
        self.LAYOUT_MAIN.addWidget(self.frame_col2, 6)
        self.LAYOUT_MAIN.addWidget(self.groupbox_col3_graphs, 4)

        self.hideButtons()  
        return

    # TODO: CHANGE THIS TO HIDE ALL CHARCTERISTIC-DEPENDENT BUTTONS AUTOMATICALLY
    def hideButtons(self): 
        self.groupbox_col2_interact.setTitle('')
        bttns = self.groupbox_col2_interact.findChildren(QPushButton)
        for btn in bttns:
            btn.hide()
        self.interact_char_table.clearContents()
        self.interact_char_table.setRowCount(0)
        self.interact_char_table.hide()

    def onRefreshBtnClick(self):
        controller.refreshBits(self.connetedItem)

    def onInitBtnClick(self):
        controller.initDatalab()
        
    def showCharTable(self,client):
        self.interact_char_table.clearContents()
        self.interact_char_table.setRowCount(0)
        row = 0
        for service in client.services:
            for char in [c for c in service.characteristics] :
                self.interact_char_table.setRowCount(row+1)
                if(char.uuid in sibel_uuid.keys()):
                    self.interact_char_table.setItem(row, 0, QTableWidgetItem(sibel_uuid[char.uuid]))
                else:
                    self.interact_char_table.setItem(row, 0, QTableWidgetItem(char.description))
                item = QTableWidgetItem(char.uuid.split('-')[0][4:])
                item.setData(0x0100, char.uuid)
                self.interact_char_table.setItem(row, 1, item)

                if 'notify' in char.properties:
                    if(char in client.notifying_characteristics.keys()):
                        notification_button = QPushButton("Disable") #TODO change button color baed on notification
                        graph_button = QPushButton('Plot')
                        graph_button.clicked.connect(self.onPlotNotificationCharClick)
                        graph_button.setMinimumHeight(10)
                        graph_button.setMinimumWidth(5)
                        graph_button.setProperty('char', char)
                        graph_button.setProperty('client', client)
                        self.interact_char_table.setCellWidget(row, 3, graph_button)
                    else:
                        notification_button = QPushButton("Enable")
                    notification_button.clicked.connect(self.onToggleNotificationCharClick)
                    notification_button.setMinimumHeight(10)
                    notification_button.setMinimumWidth(5)
                    notification_button.setProperty('char',char)
                    notification_button.setProperty('client', client)
                    self.interact_char_table.setCellWidget(row, 2, notification_button)
                
                if 'read' in char.properties:
                    read_button = QPushButton("Read")
                    read_button.clicked.connect(self.onReadCharBtnClick)
                    read_button.setMinimumHeight(10)
                    read_button.setMinimumWidth(5)
                    read_button.setProperty('char',char)
                    read_button.setProperty('client', client)
                    self.interact_char_table.setCellWidget(row, 4, read_button)

                if 'write' in char.properties:
                    write_button = QPushButton("Write")
                    write_button.clicked.connect(self.onWriteCharBtnClick)
                    write_button.setMinimumHeight(10)
                    write_button.setMinimumWidth(5)
                    write_button.setProperty('char', char)
                    write_button.setProperty('client', client)
                    self.interact_char_table.setCellWidget(row, 5, write_button)

                row += 1

        self.interact_char_table.resizeColumnsToContents()
        self.interact_char_table.resizeRowsToContents()
        self.interact_char_table.show()

    def onCompatibleClientClick(self, it):
        self.hideButtons()
        client = it.data(Qt.UserRole)
        if client.Name[0] == 'C':
            self.interact_button_battery.show()
            self.interact_button_DLInfo.show()
            self.interact_button_start_session.show()
            self.interact_button_stop_session.show()
            self.interact_button_dnld_session.show()
            self.showCharTable(client)
        if client.Name[0] == 'L':
            self.interact_button_battery.show()
            self.interact_button_DLInfo.show()
            self.interact_button_start_session.show()
            self.interact_button_stop_session.show()
            self.interact_button_dnld_session.show()
            self.showCharTable(client)
        if client.Name[0] == 'F':
            self.interact_button_battery.show()
            self.interact_button_DLInfo.show()
            self.interact_button_start_session.show()
            self.interact_button_stop_session.show()
            self.interact_button_dnld_session.show()
            self.showCharTable(client)

        self.groupbox_col2_interact.setTitle(client.Name)

    def onGetBatteryBtnClick(self):
        client = self.col1_compatible_device_table.selectedItems()[0].data(Qt.UserRole)
        task = controller.createBatteryTask(client)
        controller.executeTasks([task])

    def onGetDLInfoBtnClick(self):
        client = self.col1_compatible_device_table.selectedItems()[0].data(Qt.UserRole)
        task = controller.createDLInfoTask(client)
        controller.executeTasks([task])

    def onStartSessionBtnClick(self):
        client = self.col1_compatible_device_table.selectedItems()[0].data(Qt.UserRole)
        task = controller.createStartSessionTask(client)
        controller.executeTasks([task])

    def onStopSessionBtnClick(self):
        client = self.col1_compatible_device_table.selectedItems()[0].data(Qt.UserRole)
        task = controller.createStopSessionTask(client)
        controller.executeTasks([task])

    def onToggleNotificationCharClick(self):
        char = self.sender().property('char')
        client = self.sender().property('client')
        task = controller.createToggleNotificationTask(client, char)
        controller.executeTasks([task])
        self.showCharTable(client)

    def onPlotNotificationCharClick(self):
        char = self.sender().property('char')
        client = self.sender().property('client')
        task = controller.createPlotNotificationTask(client, char)
        controller.executeTasks([task])
        self.showCharTable(client)

    def onReadCharBtnClick(self):
        char = self.sender().property('char')
        client = self.sender().property('client')
        task = controller.createReadCharacteristicTask(client, char)
        controller.executeTasks([task])

    def onWriteCharBtnClick(self):
        char = self.sender().property('char')
        client = self.sender().property('client')
        cmd, ok = QInputDialog.getText(self, 'Write to Characteristic Task', 'Enter cmd:')
        if ok:
            try:
                cmd = bytearray.fromhex(cmd)
            except:
                msgBox = QMessageBox()
                msgBox.setText("Input a valid hex command")
                msgBox.exec()
                return
        else:
            return
        task = controller.createWriteCharacteristicTask(client, char, cmd)
        controller.executeTasks([task])

    def createNewPlotItem(self, task):
        # self.graphic_widget_col3.nextRow() # so next plot is added on another row
        # new_plot = self.graphic_widget_col3.addPlot()
        new_plot = pg.PlotWidget()
        new_layout = QGridLayout()
        new_layout.addWidget(new_plot,0,0,1,-1)

        params_button = QPushButton("Set Params") 
        params_button.clicked.connect(self.onSetPlotParamsButtonClick) 
        params_button.setProperty('task', task)
        del_button = QPushButton("Remove Plot") 
        del_button.clicked.connect(self.onRemovePlotButtonClick) 
        del_button.setProperty('task', task)

        new_layout.addWidget(params_button,1,0)
        new_layout.addWidget(del_button,1,1)

        self.layout_col3_graphs.addLayout(new_layout)
        del_button.setProperty('layout', new_layout)
        return new_plot

    def onRemovePlotButtonClick(self):
        task = self.sender().property('task')
        task.ongoing = False
        ret = controller.removeOngoingTask()
        layout = self.sender().property('layout')
        while layout.count():
            child = layout.takeAt(0)
            if child.widget():
                child.widget().deleteLater()
        self.layout_col3_graphs.update()

    def onSetPlotParamsButtonClick(self):
        task = self.sender().property('task')
        dlg = graphParamsDlg(self)
        idx = 0
        dlg.top_range.setText(str(task.parse_bytes[1]-1))
        dlg.bot_range.setText(str(task.parse_bytes[0]))
        #TODO make sure inputs from user make sense
        for shape in task.data_shape:
            dlg.table.setItem(idx, 0, QTableWidgetItem(shape.id))
            dlg.table.setItem(idx, 1, QTableWidgetItem(str(shape.no_samples)))
            dlg.table.setItem(idx, 2, QTableWidgetItem(str(shape.resolution)))
            dlg.table.setItem(idx, 3, self.getCheckbox())
            dlg.table.item(idx,3).setCheckState(Qt.CheckState(shape.signed))
            dlg.table.setItem(idx, 4, self.getCheckbox())
            dlg.table.item(idx,4).setCheckState(Qt.CheckState(shape.little_endian))
            dlg.table.setItem(idx, 5, QTableWidgetItem(str(int(1/shape.period))))
            idx += 1
        if dlg.exec():
            task.parse_bytes = (int(dlg.bot_range.text()),int(dlg.top_range.text())+1)
            new_shape = []
            for row in range(5):
                if not dlg.table.item(row,0):
                    task.setNewShape(new_shape)
                    return
                shape = [dlg.table.item(row,0).text(), int(dlg.table.item(row,1).text()), 
                    int(dlg.table.item(row,2).text()), bool(dlg.table.item(row,3).checkState()),
                    bool(dlg.table.item(row,4).checkState()), float(dlg.table.item(row,5).text())]
                new_shape.append(shape)
        else:
            pass
    
    def getCheckbox(self):
        chkBoxItem = QTableWidgetItem()
        chkBoxItem.setFlags(Qt.ItemIsUserCheckable | Qt.ItemIsEnabled)
        chkBoxItem.setCheckState(Qt.Unchecked)
        chkBoxItem.setTextAlignment(Qt.AlignCenter)
        return chkBoxItem

    def onDnldSessionBtnClick(self):
        options = QFileDialog.Options()
        options |= QFileDialog.DontUseNativeDialog
        fileName, _ = QFileDialog.getSaveFileName(self,"Choose File Name","","",options=options)
        try:
            file_name = fileName
            file_name = file_name.split('.')[0] # Removing extension in case it was selected
        except OSError as err:
            print(f"file {file_obj[0]} could not be created")
        client = self.col1_compatible_device_table.selectedItems()[0].data(Qt.UserRole)
        task = controller.createDnldSessionsTask(client, file_name)
        controller.executeTestAsync([task])

    def onChangingRegisterItemDoubleClick(self, item, col):
        if col == 3:
            cur_val = item.text(3)
            cur_client: BLEClient = item.data(10, 0)
            cur_afe:AFE = item.data(11,0)
            cur_reg:bytearray = item.data(12,0)
            # Pop up to update val
            text, ok = QInputDialog.getText(self, 'Update Register Value', 'Enter value:')
            if ok:
                controller.writeRegister(cur_client, cur_afe, cur_reg, text)

    def refreshInteractiveList(self):
        self.col1_compatible_device_table.clearContents()
        self.col1_compatible_device_table.setRowCount(0)
        connected_clients = controller.getConnectedClients()
        for client in connected_clients:
            new_row = self.col1_compatible_device_table.rowCount()
            self.col1_compatible_device_table.insertRow(new_row)
            client_item = QTableWidgetItem()
            client_item.setText(client.Name)
            client_item.setData(Qt.UserRole,client)
            self.col1_compatible_device_table.setItem(new_row, 0, client_item)
            self.col1_compatible_device_table.resizeColumnsToContents()

            # for AFE in tree[client]:
            #     AFE_item = QTreeWidgetItem(client_item)
            #     AFE_item.setText(1, device_type[AFE.type])
            #     AFE_item.setExpanded(True)
            #     AFE_item.setData(10, 0, client)
            #     for reg_add in AFE.registers:
            #         reg_add_item = QTreeWidgetItem(AFE_item)
            #         reg_add_item.setText(2, hex(reg_add))
            #         reg_add_item.setText(3, format(AFE.registers[reg_add].hex()))
            #         reg_add_item.setExpanded(True)
            #         reg_add_item.setData(11, 0, AFE)
            #         reg_add_item.setData(12, 0, AFE.registers[reg_add])
            #         reg_add_item.setData(10, 0, client)

    def reportTask(self, task):
        myFont = QFont()

        myFont.setBold(True)
        self.interact_tsk_response.setCurrentFont(myFont)
        desc = task.name + ' ON ' + task.client.Name + ':'
        self.interact_tsk_response.append(desc)

        myFont.setBold(False)
        self.interact_tsk_response.setCurrentFont(myFont)
        resp = '\t' + task.result
        self.interact_tsk_response.append(resp)

class TabWidget_Designer(QWidget):
    def __init__(self,_MainModel:DataLab_MainView):
        self.testing = False
        self._MainModel = _MainModel

        super().__init__()  
        self.layout_main = QGridLayout()
        self.setLayout(self.layout_main)

        self.groupbox_tasks = QGroupBox()
        self.groupbox_tasks.setTitle('Tasks Selection')
        self.layout_tasks = QVBoxLayout()
        self.groupbox_tasks.setLayout(self.layout_tasks)

        label_description = QLabel('Double click task to add it to the test plan')
        self.layout_tasks.addWidget(label_description)

        # Table with available tasks
        self.table_col1_available_tasks = QTableWidget()
        self.table_col1_available_tasks.setColumnCount(1)
        self.table_col1_available_tasks.setRowCount(0)
        self.table_col1_available_tasks.setHorizontalHeaderLabels(["AVAILABLE TASKS"])
        self.table_col1_available_tasks.setEditTriggers(QTableWidget.NoEditTriggers)
        self.table_col1_available_tasks.setSelectionBehavior(QAbstractItemView.SelectRows)
        self.table_col1_available_tasks.setSelectionMode(QAbstractItemView.SingleSelection)
        self.table_col1_available_tasks.itemDoubleClicked.connect(self.onTaskDoubleClick)
        self.layout_tasks.addWidget(self.table_col1_available_tasks)

        self.layout_main.addWidget(self.groupbox_tasks,0,0)

        #New col -----------------------------------

        self.groupbox_test = QGroupBox()
        self.groupbox_test.setTitle('Test Plan')
        self.layout_test = QVBoxLayout()
        self.groupbox_test.setLayout(self.layout_test)

        self.layout_plan = QHBoxLayout()
        self.btn_remove_task = QPushButton('Remove Selected Task')
        self.btn_remove_task.clicked.connect(self.onRemoveTaskBtnClick)
        self.layout_plan.addWidget(self.btn_remove_task)
        self.btn_clear_plan = QPushButton('Clear Test Plan')
        self.btn_clear_plan.clicked.connect(self.onClearTestPlanBtnClick)
        self.layout_plan.addWidget(self.btn_clear_plan)

        self.layout_test.addLayout(self.layout_plan)

        label_test_plan = QLabel('Drag and drop tasks to modify execution order')
        self.layout_test.addWidget(label_test_plan)

        #List with current test plan
        self.list_col2_test_plan = QListWidget()
        self.list_col2_test_plan.setEditTriggers(QTableWidget.NoEditTriggers)
        self.list_col2_test_plan.setSelectionBehavior(QAbstractItemView.SelectRows)
        self.list_col2_test_plan.setSelectionMode(QAbstractItemView.SingleSelection)
        self.list_col2_test_plan.setDragDropMode(QAbstractItemView.InternalMove)  
        #self.list_col2_test_plan.itemDoubleClicked.connect(self.onTaskDoubleClick)
        self.layout_test.addWidget(self.list_col2_test_plan)

        self.layout_test.addWidget(QLabel('Loading a test plan will append its content to the current test plan'))

        self.layout_save_test = QHBoxLayout()
        self.btn_save_plan = QPushButton('Save Test Plan')
        self.btn_save_plan.clicked.connect(self.onSaveTestPlanBtnClick)
        self.layout_save_test.addWidget(self.btn_save_plan)
        self.btn_loat_plan = QPushButton('Load Test Plan')
        self.btn_loat_plan.clicked.connect(self.onLoadTestPlanBtnClick)
        self.layout_save_test.addWidget(self.btn_loat_plan)

        self.layout_test.addLayout(self.layout_save_test)

        #Groupbox for test execution
        self.groupbox_execute = QGroupBox()
        self.groupbox_execute.setTitle('Test Execution')
        self.layout_execute = QGridLayout()
        self.groupbox_execute.setLayout(self.layout_execute)
        
        self.layout_execute.addWidget(QLabel('Select a device to execute the test plan on'), 0, 0)
        self.combobox_connected_devices = QComboBox()
        self.layout_execute.addWidget(self.combobox_connected_devices, 1, 0)
        self.btn_execute = QPushButton('Execute Test')
        self.btn_execute.clicked.connect(self.onExecuteTestPlanBtnClick)
        self.layout_execute.addWidget(self.btn_execute, 2, 0)

        self.layout_test.addWidget(self.groupbox_execute)

        self.layout_main.addWidget(self.groupbox_test, 0,1)

        self.layout_main.setColumnStretch(0,1)
        self.layout_main.setColumnStretch(1,3)

        self.populateAvailableTasks()

    def onSaveTestPlanBtnClick(self):
        options = QFileDialog.Options()
        options |= QFileDialog.DontUseNativeDialog
        fileName =QFileDialog.getSaveFileName(self,'Choose File Name','','*.txt',options=options)
        try:
            list_widget = self.list_col2_test_plan
            entries = '\n'.join(list_widget.item(ii).text() for ii in range(list_widget.count()))
            with open(fileName[0], 'w') as fout:
                fout.write(entries)
        except OSError as err:
            print(f"file {fileName[0]} could not be written")

    def onLoadTestPlanBtnClick(self):
        options = QFileDialog.Options()
        options |= QFileDialog.DontUseNativeDialog
        fileName = QFileDialog.getOpenFileName(self,'','','*.txt','', options)
        try:
            list_widget = self.list_col2_test_plan
            with open(fileName[0], 'r') as fin:
                entries = [e.strip() for e in fin.readlines()]
            list_widget.insertItems(0, entries)
        except OSError as err:
            with open(fileName[0], 'w'):
                pass

    def onClearTestPlanBtnClick(self):
        self.clearPlan()

    def onRemoveTaskBtnClick(self):
        it = self.list_col2_test_plan.takeItem(self.list_col2_test_plan.currentRow())
        if it:
            del(it)
        
    def onTaskDoubleClick(self, it):
        task = it.text()
        sig = controller.getTaskArgs(task)

        if 'char' in str(sig):
            dlg = selectCharacteristicDlg(self)
            if dlg.exec():
                uuid = dlg.edit_characteristic.text()
                task = task + ' on uuid: ' + uuid
            else:
                return

        if task == 'Timer':
            time, ok = QInputDialog.getText(self, 'Timer Task', 'Enter time in seconds:')
            if ok & time.isdigit():
                task = task + ' for ' + time + ' secs'
            else:
                return

        if task == 'Download Session':
            options = QFileDialog.Options()
            options |= QFileDialog.DontUseNativeDialog
            file_obj = QFileDialog.getSaveFileName(self,'','','', options=options)
            try:
                file_name = file_obj[0]
                file_name = file_name.split('.')[0] # Removing extension in case it was selected
            except OSError as err:
                print(f"file {fileName[0]} could not be created")
            task = task + ' on file: ' + file_name

        if 'Write Characteristic' in task:
            cmd, ok = QInputDialog.getText(self, 'Write to Characteristic Task', 'Enter cmd:')
            if ok:
                try:
                    cmd = bytearray.fromhex(cmd)
                except:
                    msgBox = QMessageBox()
                    msgBox.setText("Input a valid hex command")
                    msgBox.exec()
                    return
            else:
                return
            task = task + ' with cmd: {}'.format(cmd.hex())

        item = QListWidgetItem(task)
        self.list_col2_test_plan.addItem(item)

    def populateAvailableTasks(self):
        tasks_list = controller.getAvailableTasks()
        self.table_col1_available_tasks.clearContents()
        idx = 0
        for task in tasks_list:
            self.table_col1_available_tasks.insertRow(idx)
            item = QTableWidgetItem(task)
            self.table_col1_available_tasks.setItem(idx, 0, item)
            idx += 1
        self.table_col1_available_tasks.resizeRowsToContents()
        self.table_col1_available_tasks.resizeColumnsToContents()
        
    def clearPlan(self):
        self.list_col2_test_plan.clear()

    def onExecuteTestPlanBtnClick(self):
        #confirm that device is connected
        client = self.combobox_connected_devices.currentData(Qt.UserRole)
        if client in controller.getConnectedClients():
            #Desable widgets to avoid multiple executions
            self.groupbox_tasks.setEnabled(False)
            self.groupbox_test.setEnabled(False)
            self.groupbox_execute.setEnabled(True)
            # self.list_col2_test_plan.setEnabled(True)
            task_list = []
            for idx in range(self.list_col2_test_plan.count()):
                task_list.append(self.list_col2_test_plan.item(idx).text())
            controller.executeTest(client, task_list)
            self.taskidx = 0
        self.testing = True
        self._MainModel.Tab_Widget.tab2_Interactive.groupbox_col2_interact.setEnabled(False)

    def updateExecution(self, idx):
        if self.testing:
            it = self.list_col2_test_plan.item(idx)
            self.list_col2_test_plan.setCurrentItem(it)
            self.list_col2_test_plan.currentItem().setSelected(True)
            self.list_col2_test_plan.setFocus()
            self.list_col2_test_plan.repaint()
            self.taskidx += 1

    def finishExecution(self):
        self.groupbox_tasks.setEnabled(True)
        self.groupbox_test.setEnabled(True)
        self.testing = False
        self._MainModel.Tab_Widget.tab2_Interactive.groupbox_col2_interact.setEnabled(True)

    def resetView(self):
        devices = controller.getConnectedClients()
        self.combobox_connected_devices.clear()
        for device in devices:
            self.combobox_connected_devices.addItem(device.Name, device)
        
async def wakeup():
    return

class AsyncWakeup(QThread):
    signal = pyqtSignal(list)
    def __init__(self):
        super().__init__()

    def run(self):
        while True:
            time.sleep(0.3)
            self.signal.emit([])


if __name__== '__main__':
    pass