

class Crc32:
    poly = 0xedb88320
    def __init__(self):
        self.table = [0] * 16 * 256
        for i in range(256):
            res = i
            for t in range(16):
                for k in range(8):
                    res = self.poly ^ (res >> 1) if res & 1 == 1 else (res >> 1)
                    self.table[t * 256 + i] = res
    def append(self, crc, input, offset, length):
        crc_local = 0xFFFFFFFF ^ crc
        while(length >= 16):
            a = self.table[(3 * 256) + input[offset + 12]] ^ self.table[(2 * 256) + input[offset + 13]] ^ self.table[(1 * 256) + input[offset + 14]] ^ self.table[(0 * 256) + input[offset + 15]]
            b = self.table[(7 * 256) + input[offset + 8]] ^ self.table[(6 * 256) + input[offset + 9]] ^ self.table[(5 * 256) + input[offset + 10]] ^ self.table[(4 * 256) + input[offset + 11]]
            c = self.table[(11 * 256) + input[offset + 4]] ^ self.table[(10 * 256) + input[offset + 5]] ^ self.table[(9 * 256) + input[offset + 6]] ^ self.table[(8 * 256) + input[offset + 7]]
            d = self.table[(15 * 256) + (crc_local ^ input[offset]) % 256] ^ self.table[(14 * 256) + ((crc_local >> 8) ^ input[offset + 1]) % 256] ^ self.table[(13 * 256)+ ((crc_local >> 16) ^ input[offset + 2]) % 256] ^ self.table[(12 * 256) + ((crc_local >> 24) ^ input[offset+3]) % 256]
            crc_local = d ^ c ^ b ^ a
            offset += 16
            length -= 16
        length -= 1
        while(length >= 0):
            crc_local = self.table[(crc_local ^ input[offset]) % 256] ^ crc_local >> 8
            offset += 1
            length -= 1
        return crc_local ^ 0xFFFFFFFF