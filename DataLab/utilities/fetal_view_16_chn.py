import os
import csv
import ctypes as c
import numpy as np
import pandas as pd
from enum import IntEnum
import struct
import datetime
import json
from scipy import fftpack
from sklearn.decomposition import FastICA
import matplotlib.pyplot as plt
from scipy import signal
from scipy.signal import lfilter

plt.rcParams['axes.labelsize'] = 14
plt.rcParams["font.weight"] = "bold"
plt.rcParams['axes.labelweight'] = 'bold'

def butter_highpass(cutoff, fs, order=5):
    from scipy.signal import butter
    nyq = 0.5 * fs
    normal_cutoff = cutoff / nyq
    b, a = butter(order, normal_cutoff, btype='high', analog=False)
    return b, a
def butter_highpass_filter(data, cutoff, fs, order=5):
    from scipy.signal import lfilter
    b, a = butter_highpass(cutoff, fs, order=order)
    y = lfilter(b, a, data)
    return y
def butter_bandpass(lowcut, highcut, fs, order=5):
    from scipy.signal import butter
    nyq = 0.5 * fs
    low = lowcut / nyq
    high = highcut / nyq
    b, a = butter(order, [low, high], btype='band', analog=False)
    return b, a

def butter_bandpass_filter(data, lowcut, highcut, fs, order=5):
    from scipy.signal import lfilter
    b, a = butter_bandpass(lowcut, highcut, fs, order=order)
    y = lfilter(b, a, data)
    return y

class DataType(IntEnum):
    AM_MODE_5 = 5 # ACCEL X: 208hz ACCEL Y: 208hz ACCEL Z: 208hz
    BIOP_MAX3000X_MODE_1 = 10 # 512 Hz
    BIOP_ADS1299_MODE_1  = 12 # 24 bit 1000 Hz
    BIOP_ADS1299_MODE_2  = 11 # 24 bit 1000 Hz
    BIOZ_MAX3000X_MODE_1 = 21 # 64 Hz
    IMU_LSM6DSL_MODE_1   = 30 # ACCEL X: 208hz ACCEL Y: 208hz ACCEL Z: 208hz
    IMU_LSM6DSL_MODE_2   = 31 # ACCEL X: 208hz ACCEL Y: 208hz ACCEL Z: 208hz  GYRO X: 208hz GYRO Y: 208hz GYRO Z: 208hz
    IMU_LSM6DSL_MODE_3   = 32 # ACCEL X: 416hz ACCEL Y: 52hz ACCEL Z: 52hz
    PPG_MAX8614X_MODE_1  = 40 # PPG 208 Hz RED,IR
    PPG_MAX8614X_MODE_2  = 41 # PPG 208 Hz RED,IR,Green
    PPG_MAX8614X_MODE_3  = 42 # PPG 208 Hz RED,IR Transmittive and Reflective
    TEMP_MAX30205_MODE_1 = 50 # Temperature 1 Hz
    TEMP_MAX30205_MODE_3 = 52 #  Temperature .016 Hz 
    INVALID_MODE         = 255 #  Temperature .016 Hz 
    INVALID_MODE_2       = 0 #  Temperature .016 Hz 

# Sampling Period
BIOP_MAX3000X_MODE_1_PERIOD = 1.953125
BIOP_MODE_2_PERIOD = 1
BIOZ_PERIOD = 15.6239741991
MAX8614_PERIOD = 10.0
MAX8614_MODE_1_PERIOD = 3.90625
MAX8614_MODE_2_PERIOD = 3.90625
MAX8614_MODE_3_PERIOD = 3.90625
IMU_MODE3_PERIOD = 2.348772726754413# 2.348772726754413 #2.40384615385 #4.68606387941
IMU_MODE1_PERIOD = 4.68606387941
AM_MODE_5_PERIOD = 4.68606387941
MAX30205_MODE1 = 1000.0
MAX30205_MODE3 = 60000.0

# Delay
MAX30101_DSP_DELAY = -31.555
MAX8614_SENSOR_DELAY =  - 18 + 507
LSM6DSL_SENSOR_DELAY = +99.555

# ECG
ECG_GAIN = 20

# Accelometer
IMU_MODE3_SCALER = 16384.0
# Page Info
PAGE_BYTE = 2048
PRESCALER = 4.0
RTC_FREQ = 32.768

DEVICE_TYPE_CHEST = 6
DEVICE_TYPE_LIMB = 7

TEMP_LSB = 0.00390625

MAGIC_NUM = 0x37

def get_header_info(header):
    meta = {}
    
    session_id = str(struct.unpack('<Q', header[0:8])[0])
    device_type = header[12]
    epoch_time = struct.unpack('<I', header[8:12])[0]
    start_time_epoch = datetime.datetime.utcfromtimestamp(epoch_time).strftime('%Y-%m-%d %H:%M:%S')

    device_name = ""
    for c in header[14:]:
        if c == 0:
            break
        device_name += (chr(c))

    meta['sessionID'] = session_id
    meta['startSessionTime'] = start_time_epoch
    
    if device_type == DEVICE_TYPE_CHEST:
        meta['chestSensorID'] = device_name
        # meta['sessionDuration'] = session_duration
    elif device_type == DEVICE_TYPE_LIMB:
        meta['limbSensorID'] = device_name
    
    return meta

def update_info(json_path,shrd_data):

    meta_file = {} 
    try:
        with open(json_path, "r") as f:
            meta_file = json.load(f)
    except:
        print("Meta file not existing. Creating a new one")

    header_info = get_header_info(shrd_data)
    for k,v in header_info.items():
        if k not in meta_file:
            meta_file[k] = v

    with open(json_path, "w") as f:
        json.dump(meta_file, f, sort_keys=True, indent=4)

def parse_max30001_ecg512_mode1(data,data_len,epochTime,first_call,dicts,size,index,flag,adjust_sampling_rate):
    if first_call is True:
        dicts['biop'] = np.zeros((size),dtype = int)
        dicts['time(ms)'] = np.zeros((size))
        dicts['lead_off'] = np.zeros(size,dtype = bool)
    counter = 8
    i = 1.0
    while counter < 2048 :
        cur_time = epochTime - ((data_len - i) * BIOP_MAX3000X_MODE_1_PERIOD) + MAX30101_DSP_DELAY
        dicts['time(ms)'][index] = cur_time
        biop_raw = np.int64((((data[counter + 2] & 0xC0 ) >>  6) | (data[counter + 1] << 2) |(data[counter] << 10)) << 14).astype(np.int32) >> 14
        dicts['biop'][index] =  biop_raw #/(131072.0 * 20) #  (mV)  
        dicts['lead_off'][index ]= bool(flag)
        counter += 3
        i += 1
        index += 1
    return index

def parse_max30001_ads1299_mode1(data,data_len,epochTime,first_call,dicts,size,index,flag,adjust_sampling_rate):
    if first_call is True:
        dicts['biop_chn_1'] = np.zeros((size),dtype = int)
        dicts['biop_chn_2'] = np.zeros((size),dtype = int)
        dicts['biop_chn_3'] = np.zeros((size),dtype = int)
        dicts['biop_chn_4'] = np.zeros((size),dtype = int)
        dicts['biop_chn_5'] = np.zeros((size),dtype = int)
        dicts['biop_chn_6'] = np.zeros((size),dtype = int)
        dicts['biop_chn_7'] = np.zeros((size),dtype = int)
        dicts['biop_chn_8'] = np.zeros((size),dtype = int)
        dicts['time(ms)'] = np.zeros((size))
    counter = 8
    i = 1.0
    while counter < 2048 :
        cur_time = epochTime - ((data_len - i) * BIOP_MODE_2_PERIOD)
        dicts['time(ms)'][index] = cur_time
        dicts['biop_chn_1'][index]  =c.c_int32(data[counter] << 24      | data[counter + 1] << 16  | data[counter + 2] << 8).value  >> 8
        dicts['biop_chn_2'][index]  =c.c_int32(data[counter + 3] << 24  | data[counter + 4] << 16  | data[counter + 5] << 8).value  >> 8
        dicts['biop_chn_3'][index]  =c.c_int32(data[counter + 6] << 24  | data[counter + 7] << 16  | data[counter + 8] << 8).value  >> 8
        dicts['biop_chn_4'][index]  =c.c_int32(data[counter + 9] << 24  | data[counter + 10] << 16 | data[counter + 11] << 8).value  >> 8
        dicts['biop_chn_5'][index]  =c.c_int32(data[counter + 12] << 24 | data[counter + 13] << 16 | data[counter + 14] << 8).value  >> 8
        dicts['biop_chn_6'][index]  =c.c_int32(data[counter + 15] << 24 | data[counter + 16] << 16 | data[counter + 17] << 8).value  >> 8
        dicts['biop_chn_7'][index]  =c.c_int32(data[counter + 18] << 24 | data[counter + 19] << 16 | data[counter + 20] << 8).value  >> 8
        dicts['biop_chn_8'][index]  =c.c_int32(data[counter + 21] << 24 | data[counter + 22] << 16 | data[counter + 23] << 8).value  >> 8
        counter += 24
        i += 1
        index += 1
    return index

def parse_max30001_ads1299_mode2(data,data_len,epochTime,first_call,dicts,size,index,flag,adjust_sampling_rate):
    if first_call is True:
        dicts['biop_chn_1'] = np.zeros((size),dtype = int)
        dicts['biop_chn_2'] = np.zeros((size),dtype = int)
        dicts['biop_chn_3'] = np.zeros((size),dtype = int)
        dicts['biop_chn_4'] = np.zeros((size),dtype = int)
        dicts['biop_chn_5'] = np.zeros((size),dtype = int)
        dicts['biop_chn_6'] = np.zeros((size),dtype = int)
        dicts['biop_chn_7'] = np.zeros((size),dtype = int)
        dicts['biop_chn_8'] = np.zeros((size),dtype = int)
        dicts['biop_chn_9'] = np.zeros((size),dtype = int)
        dicts['biop_chn_10'] = np.zeros((size),dtype = int)
        dicts['biop_chn_11'] = np.zeros((size),dtype = int)
        dicts['biop_chn_12'] = np.zeros((size),dtype = int)
        dicts['biop_chn_13'] = np.zeros((size),dtype = int)
        dicts['biop_chn_14'] = np.zeros((size),dtype = int)
        dicts['biop_chn_15'] = np.zeros((size),dtype = int)
        dicts['biop_chn_16'] = np.zeros((size),dtype = int)
        dicts['time(ms)'] = np.zeros((size))
    counter = 8
    i = 1.0
    while counter < 2016 :
        cur_time = epochTime - ((data_len - i) * BIOP_MODE_2_PERIOD)
        dicts['time(ms)'][index] = cur_time
        dicts['biop_chn_1'][index]  =c.c_int32(data[counter] << 24      | data[counter + 1] << 16  | data[counter + 2] << 8).value  >> 8
        dicts['biop_chn_2'][index]  =c.c_int32(data[counter + 3] << 24  | data[counter + 4] << 16  | data[counter + 5] << 8).value  >> 8
        dicts['biop_chn_3'][index]  =c.c_int32(data[counter + 6] << 24  | data[counter + 7] << 16  | data[counter + 8] << 8).value  >> 8
        dicts['biop_chn_4'][index]  =c.c_int32(data[counter + 9] << 24  | data[counter + 10] << 16 | data[counter + 11] << 8).value  >> 8
        dicts['biop_chn_5'][index]  =c.c_int32(data[counter + 12] << 24 | data[counter + 13] << 16 | data[counter + 14] << 8).value  >> 8
        dicts['biop_chn_6'][index]  =c.c_int32(data[counter + 15] << 24 | data[counter + 16] << 16 | data[counter + 17] << 8).value  >> 8
        dicts['biop_chn_7'][index]  =c.c_int32(data[counter + 18] << 24 | data[counter + 19] << 16 | data[counter + 20] << 8).value  >> 8
        dicts['biop_chn_8'][index]  =c.c_int32(data[counter + 21] << 24 | data[counter + 22] << 16 | data[counter + 23] << 8).value  >> 8
        dicts['biop_chn_9'][index]  =c.c_int32(data[counter + 24] << 24 | data[counter + 25] << 16 | data[counter + 26] << 8).value  >> 8
        dicts['biop_chn_10'][index] =c.c_int32(data[counter + 27] << 24 | data[counter + 28] << 16 | data[counter + 29] << 8).value  >> 8
        dicts['biop_chn_11'][index] =c.c_int32(data[counter + 30] << 24 | data[counter + 31] << 16 | data[counter + 32] << 8).value  >> 8
        dicts['biop_chn_12'][index] =c.c_int32(data[counter + 33] << 24 | data[counter + 34] << 16 | data[counter + 35] << 8).value  >> 8
        dicts['biop_chn_13'][index] =c.c_int32(data[counter + 36] << 24 | data[counter + 37] << 16 | data[counter + 38] << 8).value  >> 8
        dicts['biop_chn_14'][index] =c.c_int32(data[counter + 39] << 24 | data[counter + 40] << 16 | data[counter + 41] << 8).value  >> 8
        dicts['biop_chn_15'][index] =c.c_int32(data[counter + 42] << 24 | data[counter + 43] << 16 | data[counter + 44] << 8).value  >> 8
        dicts['biop_chn_16'][index] =c.c_int32(data[counter + 45] << 24 | data[counter + 46] << 16 | data[counter + 47] << 8).value  >> 8
        counter += 48
        i += 1
        index += 1
    return index
def parse_max30001_bioz_mode1(data,data_len,epochTime,first_call,dicts,size,index,flag,adjust_sampling_rate):
    if first_call is True:
        dicts['bioz'] = np.zeros((size),dtype = int)
        dicts['time(ms)'] = np.zeros((size))
    counter = 8
    i = 1.0
    while counter < data_len * 3 + 8 :
        cur_time = epochTime - (data_len - i) * BIOZ_PERIOD
        dicts['time(ms)'][index] = cur_time
        dicts['bioz'][index] = np.int32(((((data[counter + 2] & 0xF0) >> 4) | (data[counter + 1] << 4) | (data[counter] << (12))) << 12) >>12)
        index += 1
        counter += 3
        i += 1
    return index

def parse_max8614_mode1(data,data_len,epochTime,first_call,dicts,size,index,flag,adjust_sampling_rate):
    if first_call is True:
        dicts['ir_transmissive'] = np.zeros((size),dtype = int)
        dicts['red_transmissive'] = np.zeros((size),dtype = int)
        dicts['time(ms)'] = np.zeros((size))
        dicts['detached'] = np.zeros((size),dtype = bool)
    counter = 8
    i = 1.0

    flipped = False
    tag = data[8] >> 3
    if tag != 1 and tag != 13:
        flipped = True

    while counter < 2048 :
        if adjust_sampling_rate is not None:
            cur_time = epochTime - (data_len - i) * adjust_sampling_rate + MAX8614_SENSOR_DELAY
        else:
            cur_time = epochTime - (data_len - i) * MAX8614_MODE_1_PERIOD + MAX8614_SENSOR_DELAY
        dicts['time(ms)'][index] = cur_time

        if flipped is False:
            dicts['red_transmissive'][index] = ((data[counter] & 0x7) << 16) | (data[counter + 1] << 8) | data[counter + 2]
            dicts['ir_transmissive'][index] = ((data[counter + 3] & 0x7) << 16) | (data[counter + 4] << 8) | data[counter + 5]
        if flipped is True:
            dicts['ir_transmissive'][index] = ((data[counter] & 0x7) << 16) | (data[counter + 1] << 8) | data[counter + 2]
            dicts['red_transmissive'][index] = ((data[counter + 3] & 0x7) << 16) | (data[counter + 4] << 8) | data[counter + 5]
        dicts['detached'][index] = bool(flag)
        counter += 6
        i += 1
        index += 1
    return index

def parse_max8614_mode2(data,data_len,epochTime,first_call,dicts,size,index,flag,adjust_sampling_rate):
    if first_call is True:
        dicts['red_transmissive'] = np.zeros((size),dtype = int)
        dicts['ir_transmissive'] = np.zeros((size),dtype = int)
        dicts['green_transmissive'] = np.zeros((size),dtype = int)
        dicts['time(ms)'] = np.zeros((size))
        dicts['detached'] = np.zeros((size),dtype = bool)
    counter = 8
    i = 1.0
    while counter < 2025 :
        if adjust_sampling_rate is not None:
            cur_time = epochTime - (data_len - i) * adjust_sampling_rate + MAX8614_SENSOR_DELAY
        else:
            cur_time = epochTime - (data_len - i) * MAX8614_MODE_2_PERIOD + MAX8614_SENSOR_DELAY
        dicts['time(ms)'][index] = cur_time
        dicts['red_transmissive'][index] = ((data[counter] & 0x7) << 16) | (data[counter + 1] << 8) | data[counter + 2]
        dicts['ir_transmissive'][index] = ((data[counter + 3] & 0x7) << 16) | (data[counter + 4] << 8) | data[counter + 5]
        dicts['green_transmissive'][index] = ((data[counter + 6] & 0x7) << 16) | (data[counter +7] << 8) | data[counter + 8]
        dicts['detached'][index] = bool(flag)
        counter += 9
        i += 1
        index += 1
    return index

def parse_max8614_mode3(data,data_len,epochTime,first_call,dicts,size,index,flag,adjust_sampling_rate):
    if first_call is True:
        dicts['ir_transmissive'] = np.zeros((size),dtype = int)
        dicts['ir_reflective'] = np.zeros((size),dtype = int)
        dicts['red_transmissive'] = np.zeros((size),dtype = int)
        dicts['red_reflective'] = np.zeros((size),dtype = int)
        dicts['time(ms)'] = np.zeros((size))
        dicts['detached'] = np.zeros((size),dtype = bool)
    counter = 8
    i = 1.0
    while counter < 2048 :
        if adjust_sampling_rate is not None:
            cur_time = epochTime - (data_len - i) * adjust_sampling_rate + MAX8614_SENSOR_DELAY
        else:
            cur_time = epochTime - (data_len - i) * MAX8614_MODE_3_PERIOD + MAX8614_SENSOR_DELAY
        dicts['time(ms)'][index] = cur_time
        dicts['red_transmissive'][index] = ((data[counter] & 0x7) << 16) | (data[counter + 1] << 8) | data[counter + 2]
        dicts['red_reflective'][index] = ((data[counter + 3] & 0x7) << 16) | (data[counter + 4] << 8) | data[counter + 5]
        dicts['ir_transmissive'][index] = ((data[counter + 6] & 0x7) << 16) | (data[counter +7] << 8) | data[counter + 8]
        dicts['ir_reflective'][index] = ((data[counter + 9] & 0x7) << 16) | (data[counter + 10] << 8) | data[counter + 11]
        dicts['detached'][index] = bool(flag)
        counter += 12
        i += 1
        index += 1
    return index

def parse_lsm6dsl_imu208_mode3(data,data_len,epochTime,first_call,dicts,size,index,flag,adjust_sampling_rate):
    if first_call is True:
        # writer.writerow(["time","x","y","z"])
        dicts['xl_x'] = np.zeros(size,dtype = float)
        dicts['xl_y'] = np.zeros(size,dtype = float)
        dicts['xl_z'] = np.zeros(size,dtype = float)
        dicts['time(ms)'] = np.zeros(size)
    counter = 8
    i = 1.0
    while counter < 2048:
        if adjust_sampling_rate is not None:
            cur_time = epochTime - (data_len - i) * adjust_sampling_rate + LSM6DSL_SENSOR_DELAY
        else:
            cur_time = epochTime - (data_len - i) * IMU_MODE3_PERIOD + LSM6DSL_SENSOR_DELAY
        dicts['time(ms)'][index] = cur_time
        if(counter - 22) % 20 == 0:
            dicts['xl_x'][index] = c.c_int16(data[counter] | (data[counter + 1] << 8)).value/IMU_MODE3_SCALER
            dicts['xl_y'][index] = c.c_int16(data[counter + 2] | (data[counter + 3] << 8)).value/IMU_MODE3_SCALER
            dicts['xl_z'][index] = c.c_int16(data[counter + 4] | (data[counter + 5] << 8)).value/IMU_MODE3_SCALER
            counter += 6
        else:
            dicts['xl_x'][index] = np.nan
            dicts['xl_y'][index] = np.nan
            dicts['xl_z'][index] = c.c_int16(data[counter] | (data[counter + 1] << 8)).value/IMU_MODE3_SCALER
            counter += 2
        i += 1
        index += 1
    return index

def parse_lsm6dsl_imu208_mode1(data,data_len,epochTime,first_call,dicts,size,index,flag,adjust_sampling_rate):
    if first_call is True:
        dicts['xl_x'] = np.zeros((size),dtype = int)
        dicts['xl_y'] = np.zeros((size),dtype = int)
        dicts['xl_z'] = np.zeros((size),dtype = int)
        dicts['time(ms)'] = np.zeros(size)
    counter = 8
    i = 1.0
    while counter < 2048 :
        cur_time = epochTime - (data_len - i) * IMU_MODE1_PERIOD + LSM6DSL_SENSOR_DELAY
        dicts['time(ms)'][index] = cur_time
        dicts['xl_x'][index] = c.c_int16(data[counter] | (data[counter + 1] << 8)).value
        dicts['xl_y'][index] = c.c_int16(data[counter + 2] | (data[counter + 3] << 8)).value
        dicts['xl_z'][index] = c.c_int16(data[counter + 4] | (data[counter + 5] << 8)).value
        counter += 6
        i += 1
        index += 1
    return index

def parse_am_mode5(data,data_len,epochTime,first_call,dicts,size,index,flag,adjust_sampling_rate):
    if first_call is True:
        dicts['xl_x'] = np.zeros((size))
        dicts['xl_y'] = np.zeros((size))
        dicts['xl_z'] = np.zeros((size))
        dicts['time(ms)'] = np.zeros(size)
    counter = 8
    i = 1.0
    while counter < 2048 :

        cur_time = (epochTime / PRESCALER * RTC_FREQ)- (data_len - i) * AM_MODE_5_PERIOD
        dicts['time(ms)'][index] = cur_time
        if(counter - 14) % 12 == 0:
            dicts['xl_x'][index] = c.c_int16(data[counter] | (data[counter + 1] << 8)).value
            dicts['xl_y'][index] = c.c_int16(data[counter + 2] | (data[counter + 3] << 8)).value
            dicts['xl_z'][index] = c.c_int16(data[counter + 4] | (data[counter + 5] << 8)).value
            counter += 6
        else:
            dicts['xl_x'][index] = np.nan
            dicts['xl_y'][index] = np.nan
            dicts['xl_z'][index] = c.c_int16(data[counter] | (data[counter + 1] << 8)).value
            counter += 2
        i += 1
        index += 1
    return index

def parse_temp_max30205_mode1(data,data_len,epochTime,first_call,dicts,size,index,flag,adjust_sampling_rate):
    if first_call is True:
        dicts['time(ms)'] = np.zeros((size),dtype = int)
        dicts['temp'] = np.zeros(size)
    counter = 8
    i = 1.0

    while counter < data_len * 2 + 8:
        cur_time = epochTime - (data_len - i) * MAX30205_MODE1
        raw = c.c_int16((data[counter] << 8) | (data[counter + 1])).value
        temp = raw * TEMP_LSB
        temp = int(temp * 10)
        temp = temp * .01

        dicts['time(ms)'][index] = cur_time
        dicts['temp'][index] = temp
        index += 1
        counter += 2
        i += 1
    return index

def parse_temp_max30205_mode3(data,data_len,epochTime,first_call,dicts,size,index,flag,adjust_sampling_rate):
    if first_call is True:
        dicts['time(ms)'] = np.zeros((size),dtype = int)
        dicts['temp'] = np.zeros(size)
    counter = 8
    i = 1.0

    while counter < data_len * 2 + 8:
        cur_time = epochTime - (data_len - i) * MAX30205_MODE3
        raw = c.c_int16((data[counter] << 8) | (data[counter + 1])).value
        temp = raw * TEMP_LSB
        temp = int(temp * 10)
        temp = temp * .01

        dicts['time(ms)'][index] = cur_time
        dicts['temp'][index] = temp
        index += 1
        counter += 2
        i += 1
    return index

def getSize(filename):
    if os.path.exists(filename) is False:
        return 0
    st = os.path.getsize(filename)
    return st

options = { DataType.BIOP_MAX3000X_MODE_1 : parse_max30001_ecg512_mode1,
            DataType.BIOP_ADS1299_MODE_1  : parse_max30001_ads1299_mode2,
            DataType.BIOP_ADS1299_MODE_2  : parse_max30001_ads1299_mode2,
            DataType.BIOZ_MAX3000X_MODE_1 : parse_max30001_bioz_mode1,
            DataType.PPG_MAX8614X_MODE_1  : parse_max8614_mode1,
            DataType.PPG_MAX8614X_MODE_2  : parse_max8614_mode2,
            DataType.PPG_MAX8614X_MODE_3  : parse_max8614_mode3,
            DataType.IMU_LSM6DSL_MODE_1   : parse_lsm6dsl_imu208_mode1,
            DataType.IMU_LSM6DSL_MODE_3   : parse_lsm6dsl_imu208_mode3,
            DataType.TEMP_MAX30205_MODE_1 : parse_temp_max30205_mode1,
            DataType.AM_MODE_5            : parse_am_mode5,
            DataType.TEMP_MAX30205_MODE_3 : parse_temp_max30205_mode3
}

index_per_page = { DataType.BIOP_MAX3000X_MODE_1 : 680,
                   DataType.BIOP_ADS1299_MODE_1  : 42,
                   DataType.BIOP_ADS1299_MODE_2  : 42,
                   DataType.PPG_MAX8614X_MODE_1  : 340,
                   DataType.PPG_MAX8614X_MODE_2  : 225,
                   DataType.PPG_MAX8614X_MODE_3  : 170,
                   DataType.IMU_LSM6DSL_MODE_3   : 816,
                   DataType.TEMP_MAX30205_MODE_1 : 1020,
                   DataType.AM_MODE_5            : 680,
                   DataType.TEMP_MAX30205_MODE_3 : 1020,
                   DataType.INVALID_MODE : 0,
                   DataType.INVALID_MODE_2 : 0
}


def parse_page(data,first_call,dicts,size,index, adjust_sampling_rate):
    datatype = data[0]
    flag = data[1]
    data_len = ((data[3] << 8) + data[2])
    epochTime = ((data[7] << 24) | (data[6] << 16) | (data[5] << 8) | data[4]) * PRESCALER / RTC_FREQ
    if(options.get(datatype) is not None):
        return options[datatype](data,data_len,epochTime,first_call,dicts,size,index,flag, adjust_sampling_rate)
        

def get_size_dict(dict,total_file,total_size):

    for i in range(0, int(total_size / PAGE_BYTE)):
        datatype = total_file[i * PAGE_BYTE]
        if datatype not in dict:
            dict[datatype] = 0
        if datatype == DataType.TEMP_MAX30205_MODE_1:
            dict[datatype] = dict[datatype] + ((total_file[i * PAGE_BYTE + 3] << 8) + total_file[i * PAGE_BYTE + 2])
        else:
            dict[datatype] = dict[datatype] + index_per_page[datatype]#2048#((total_file[i * PAGE_BYTE + 3] << 8) + total_file[i * PAGE_BYTE + 2])

def get_sampling_rate(sampling_rate,shrd_file,total_size):

    page_epoch_time = {}
    for i in range(0, int(total_size / PAGE_BYTE)):
        datatype = shrd_file[i * PAGE_BYTE]
        epochTime = ((shrd_file[i * PAGE_BYTE + 7] << 24) | (shrd_file[i * PAGE_BYTE + 6] << 16) | (shrd_file[i * PAGE_BYTE + 5] << 8) | shrd_file[i * PAGE_BYTE + 4]) * PRESCALER / RTC_FREQ

        if datatype not in page_epoch_time:
            page_epoch_time[datatype] = []

        page_epoch_time[datatype].append(epochTime)

    # Get Sampling Rate    
    for k,v in page_epoch_time.items():
        if len(v) > 10:
            sorted_time_diff = np.sort(np.diff(v))
            filtered_time_diff = sorted_time_diff[int(len(v)/3):-1*int(len(v)/3)]

            data_sampling_rate = np.average(filtered_time_diff)/index_per_page[k]
            sampling_rate[k] = data_sampling_rate 
        else:
            sampling_rate[k] = None 

def get_dict_df(filename, processed_dir = None, extract_sampling_rate = False):


    file_bytes_array = open(filename,"rb").read()
    total_size = getSize(filename)
    if(total_size == 0):
        print("File does not exist or there is no content in the file")
        return None
    first_call = False
    if total_size % PAGE_BYTE != 0:
        if total_size % PAGE_BYTE == 26:

            if processed_dir == None:
                processed_dir = os.path.dirname(filename)
            print("Header Info Found")
            update_info(os.path.join(processed_dir,"info.json"),file_bytes_array[:26])
            file_bytes_array = file_bytes_array[26:]
        else:
            file_bytes_array = file_bytes_array[total_size % PAGE_BYTE:]
    print("File size is ",total_size)

    dicts_per_datatype = {}

    size_per_datatype = {}
    sampling_rate_per_datatype = {}

    index_per_datatype = {}
    get_size_dict(size_per_datatype,file_bytes_array,total_size)

    if extract_sampling_rate:
        get_sampling_rate(sampling_rate_per_datatype, file_bytes_array,total_size)

    # for key in size_per_datatype:
    #     print(key,size_per_datatype[key])
    for i in range(0, int(total_size / PAGE_BYTE) ):
        datatype = file_bytes_array[i * PAGE_BYTE]
        first_call = False

        if dicts_per_datatype.get(datatype) is None:
            if datatype == DataType.BIOP_MAX3000X_MODE_1:
                dicts_per_datatype[DataType.BIOP_MAX3000X_MODE_1] = {}
                index_per_datatype[DataType.BIOP_MAX3000X_MODE_1] = 0
                first_call = True
            elif datatype == DataType.BIOP_ADS1299_MODE_1:
                dicts_per_datatype[DataType.BIOP_ADS1299_MODE_1] = {}
                index_per_datatype[DataType.BIOP_ADS1299_MODE_1] = 0
                first_call = True
            elif datatype == DataType.BIOP_ADS1299_MODE_2:
                dicts_per_datatype[DataType.BIOP_ADS1299_MODE_2] = {}
                index_per_datatype[DataType.BIOP_ADS1299_MODE_2] = 0
                first_call = True
            elif datatype == DataType.BIOZ_MAX3000X_MODE_1:
                dicts_per_datatype[DataType.BIOZ_MAX3000X_MODE_1] = {}
                index_per_datatype[DataType.BIOZ_MAX3000X_MODE_1] = 0
                first_call = True
            elif datatype == DataType.PPG_MAX8614X_MODE_1:
                dicts_per_datatype[DataType.PPG_MAX8614X_MODE_1] = {}
                index_per_datatype[DataType.PPG_MAX8614X_MODE_1] = 0
                first_call = True
            elif datatype == DataType.PPG_MAX8614X_MODE_2:
                dicts_per_datatype[DataType.PPG_MAX8614X_MODE_2] = {}
                index_per_datatype[DataType.PPG_MAX8614X_MODE_2] = 0
                first_call = True
            elif datatype == DataType.PPG_MAX8614X_MODE_3:
                dicts_per_datatype[DataType.PPG_MAX8614X_MODE_3] = {}
                index_per_datatype[DataType.PPG_MAX8614X_MODE_3] = 0
                first_call = True
            elif datatype == DataType.IMU_LSM6DSL_MODE_1:
                dicts_per_datatype[DataType.IMU_LSM6DSL_MODE_1] = {}
                index_per_datatype[DataType.IMU_LSM6DSL_MODE_1] = 0
                first_call = True
            elif datatype == DataType.IMU_LSM6DSL_MODE_3:
                dicts_per_datatype[DataType.IMU_LSM6DSL_MODE_3] = {}
                index_per_datatype[DataType.IMU_LSM6DSL_MODE_3] = 0
                first_call = True
            elif datatype == DataType.TEMP_MAX30205_MODE_1:
                dicts_per_datatype[DataType.TEMP_MAX30205_MODE_1] = {}
                index_per_datatype[DataType.TEMP_MAX30205_MODE_1] = 0
                first_call = True
            elif datatype == DataType.AM_MODE_5:
                dicts_per_datatype[DataType.AM_MODE_5] = {}
                index_per_datatype[DataType.AM_MODE_5] = 0
                first_call = True
            elif datatype == DataType.TEMP_MAX30205_MODE_3:
                dicts_per_datatype[DataType.TEMP_MAX30205_MODE_3] = {}
                index_per_datatype[DataType.TEMP_MAX30205_MODE_3] = 0
                first_call = True
            else:
                continue
        data = file_bytes_array[i*PAGE_BYTE:(i+1)*PAGE_BYTE]

        if datatype in sampling_rate_per_datatype:
            index_per_datatype[datatype] = parse_page(data,first_call,dicts_per_datatype[datatype],size_per_datatype[datatype],index_per_datatype[datatype], sampling_rate_per_datatype[datatype])
        else:
            index_per_datatype[datatype] = parse_page(data,first_call,dicts_per_datatype[datatype],size_per_datatype[datatype],index_per_datatype[datatype], None)
    dicts_df = {}
    for key in dicts_per_datatype.keys():
        new_df = pd.DataFrame.from_dict(dicts_per_datatype[key])
        dicts_df[key] = new_df
    return dicts_df 

if __name__ == "__main__":
    fs = 1000.0  # Sample frequency (Hz)
    f0 = 60.0  # Frequency to be removed from signal (Hz)
    Q = 12.0  # Quality factor

    b_60hz, a_60hz = signal.iirnotch(f0, Q, fs)
    b_9_5hz, a_9_5hz = signal.iirnotch(9.5, Q, fs)
    b_11_7hz, a_11_7hz = signal.iirnotch(11.7, Q, fs)


    # Read Data
    y = get_dict_df("data_collection_20201110/no_bias.shrd")
    y[DataType.BIOP_ADS1299_MODE_1][5000:].to_csv("data_collection_20201110/test2.csv", index =False)
    print("HIHI")
    print(list(y))
    for i in range(1,2):
        CHN = i
        # Filter
        # data2 = lfilter(b_60hz,a_60hz,y[DataType.BIOP_ADS1299_MODE_1][f'biop_chn_{CHN}']) # powerline
        data2 = y[DataType.BIOP_ADS1299_MODE_1][f'biop_chn_{CHN}']

        VREF = 4.5
        GAIN = 24
        SCALE_TO_UVOLT = 0.0000000121 # (VREF/GAIN)/(2**24)
        # SCALE_TO_UVOLT = (VREF/GAIN)/(2**23)
        data2 = data2 * SCALE_TO_UVOLT

        # Graph
        # plt.title("no 60 Hz filter applied")
        # plt.plot(y[DataType.BIOP_ADS1299_MODE_1]['time(ms)'], butter_bandpass_filter(data2,1.0,70,1000,3) * 1000 *1000- 50000 * i,linewidth = 0.8, label = f'biop_chn_{CHN}')
        # plt.subplot(211)

        # plt.plot(data2* 1000- 100 * i,linewidth = 0.8, label = f'biop_chn_{CHN}')
        # plt.plot(data2* 1000- 100 * (i-1),linewidth = 0.8, label = f'biop_chn_{CHN}')
        # print([i for i,e in enumerate(data2.values) if e == 0].diff())
        plt.plot(([e for i,e in enumerate(data2.values) if e != 0]) ,linewidth = 0.8, label = f'biop_chn_{CHN}')
        # plt.plot(data2 ,linewidth = 0.8, label = f'biop_chn_{CHN}')
        # plt.plot(y[DataType.BIOP_ADS1299_MODE_1]['time(ms)'], data2* 1000- 50 * i,linewidth = 0.8, label = f'biop_chn_{CHN}')
        # plt.xlabel("Time(ms)")
        # plt.ylabel("Voltage(mV)")
        # plt.plot(y[DataType.BIOP_ADS1299_MODE_2]['time(ms)']/1000,data2* 1000 - 1 * i,linewidth = 0.8, label = f'biop_chn_{CHN}')
        # plt.plot(data2* 1000 - 1 * i,linewidth = 0.8, label = f'biop_chn_{CHN}')
        # plt.plot(data2,linewidth = 0.8)
        # plt.plot(data2,linewidth = 0.8)
        # plt.subplot(212)
        # sig_fft = fftpack.fft(data2)
        # power = np.abs(sig_fft)
        # sample_freq = fftpack.fftfreq(9000, d=0.001)
        # plt.plot(sample_freq[:4500],power[:4500])
    # plt.legend()

    plt.show()
