from .definitions import *
import time
import numpy as np
import pyqtgraph as pg
from bitarray import bitarray 
from bitarray.util import int2ba, ba2int
from os.path import exists
from copy import copy

class baseTask:
    subclasses = {}

    @classmethod
    def register_subclass(self, task_type):
        def decorator(subclass):
            self.subclasses[task_type] = subclass
            return subclass

        return decorator

    @classmethod
    def create(self, task_type, params):
        if task_type not in self.subclasses:
            raise ValueError('Bad task type {}'.format(task_type))
            return None

        return self.subclasses[task_type](*params)

class Task(baseTask):   
    def __init__(self, client, seq:list):
        self.sequence = seq # List of tuples containing the operations and necesary information to execute the whole task. Last operation in this list should return the result
        self.client = client
        self.__result:str = None
        self.resultSize = -1 # Number of expected bytes in the result
        self.name = 'Generic Task'
        self.ongoing = False
        self.done = True
        self.continue_with = False

    @property
    def result(self):
        return self.__result

    @result.setter
    def result(self, res):
        if isinstance(res, bytearray):
            self.__result = res[:self.resultSize].hex()
        if type(res) == str:
            self.__result = res
        if res == False:
            self.__result = 'Execution Error'
        elif res == True:
            self.__result = 'Done'

@baseTask.register_subclass('Read Characteristic')
class ReadTask(Task):
    def __init__(self, client, char):
        super().__init__(client, [('r', char, None)])
        self.name = 'Read-Characteristic Task'

@baseTask.register_subclass('Write Characteristic')
class WriteTask(Task):
    def __init__(self, client, char, cmd:bytearray):
        super().__init__(client, [('w', char, cmd)])
        self.name = 'Write-Characteristic Task'
        self.char = char
        self.cmd = cmd
    
    @Task.result.setter
    def result(self, res:bool):
        if res:
            res = 'Wrote command {} to {}'.format(self.cmd.hex(), self.char)
        else:
            res = 'Error writting command'
        Task.result.fset(self, res)

@baseTask.register_subclass('DL Info')
class DLInfoTask(Task):
    def __init__(self, client):
        super().__init__(client, dlinfo_task_sequence)
        self.resultSize = 6
        self.name = 'Get-Data-Logging-Info Task'

    @Task.result.setter
    def result(self, res):
        if type(res)==bytearray:
            res = res[:self.resultSize]
            num_pages = int.from_bytes(res[:4],byteorder='little')
            flag = res[4]
            nand_status:bool = res[5]
            res = 'No Pages: ' + str(num_pages) + ' - Session Status: ' + (session_status_descriptor[flag] if flag in session_status_descriptor.keys() else str(flag)) + ' - NAND Status: ' + str(nand_status)
        Task.result.fset(self, res)
 
@baseTask.register_subclass('Battery Level') 
class BatteryLevelTask(Task):
    def __init__(self, client):
        super().__init__(client, battery_task_sequence)
        self.resultSize = 1 
        self.name = 'Get-Battery-Level Task'

    # @property
    # def result(self):
    #     return self.__result

    @Task.result.setter
    def result(self, res:bytearray):
        if type(res)==bytearray:
            res = res[:self.resultSize]
            res = int.from_bytes(res,byteorder='big')
            if res>100:
                res-=128
            res = str(res) + '%'
        Task.result.fset(self, res)

@baseTask.register_subclass('Start Session')
class StartSessionTask(Task):
    def __init__(self,client):
        cmd = bytearray.fromhex("01") + int(time.time()).to_bytes(4, 'little') + bytearray.fromhex("000000000000")
        start_session_task_sequence = [
            ('w', sibel_cmd_uuid, cmd),
            ('r', sibel_sys_stream, None)
        ]   
        super().__init__(client,start_session_task_sequence)
        self.name = 'Start-Session Task'

    @Task.result.setter
    def result(self, res:bool):
        if res:
            res = 'Session Started'
        else:
            res = 'Error starting the session'
        Task.result.fset(self, res)

@baseTask.register_subclass('Stop Session')
class StopSessionTask(Task):
    def __init__(self,client):  
        super().__init__(client, stop_session_task_sequence)
        self.name = 'Stop-Session Task'

    @Task.result.setter
    def result(self, res:bool):
        if res:
            res = 'Session Stopped'
        else:
            res = 'Error stopping the session'
        Task.result.fset(self, res)

@baseTask.register_subclass('Toggle Notification')
class ToggleNotificationTask(Task):
    def __init__(self, client, char:str):
        super().__init__(client, [('n', char, None)])
        self.name = 'Toggle-Notification Task'

    @Task.result.setter
    def result(self, res:bool):
        if res:
            res = 'Notifying'
        else:
            res = 'Not Notifying'
        Task.result.fset(self, res)

class OngoingTask(Task):
    def __init__(self, client, seq=[], generator=None):
        super().__init__(client, seq)
        self.name = 'Ongoing Task'
        self.ongoing = True
        self.generator = generator # Logical str that represents whatever triggers the task. Usually a characteristic uuid
    
    def trigger(self):
        # Called when the 'generator' executes the task
        print(self.name + ' Triggered')

    @Task.result.setter
    def result(self, res:bool):
        if res:
            res = 'Running'
        else:
            res = 'Not Running'
        Task.result.fset(self, res)

@baseTask.register_subclass('Plot Notification')
class PlotNotificationTask(OngoingTask):
    def __init__(self, client, char:str):
        super().__init__(client, [('p', char, None)], char)
        self.name = 'Plot-Notification Task'
        self.plotitem = None
        self.data_shape = [SampleShape()]
        self.parse_bytes = (0,4)

    def setPlot(self, plotitem):
        self.plotitem = plotitem
        title = str(self.client.Name) + ' on ' + self.generator.split('-')[0][4:] + ' Notification'
        self.plotitem.setTitle(title)
        self.plotitem.setRange(xRange=[-100, 0])
        self.plotitem.setLimits(xMax=0)  
        self.curves = []
        self.data = []   
        self.times = []  
        self.curves_dict = {}
        self.time = 0
        idx = 0
        for cycle_shape in self.data_shape:
            if cycle_shape.id not in self.curves_dict.keys():
                self.curves_dict[cycle_shape.id] = idx
                self.curves.append(self.plotitem.plot())
                self.data.append([])
                self.times.append([])
                idx += 1
        return True

    def trigger(self, data:bytearray):
        # Called when the specific characteristic, as a generator, notifies new data
        if self.plotitem:
            curves = self.parseNotification(data) # TODO handle parsing errors!!!
            for idx in self.curves_dict.values():
                self.data[idx] = np.append(self.data[idx], curves[idx])
                new_time = (self.time ) + ((self.data_shape[idx].no_samples-1) * (self.data_shape[idx].period))
                times = np.linspace(self.time, new_time, self.data_shape[idx].no_samples)
                self.times[idx] = np.append(self.times[idx], times)
                self.curves[idx].setData(x=self.times[idx], y=self.data[idx])
                self.curves[idx].setPos(-self.data[idx].shape[0], 0)
            self.time = (new_time + self.data_shape[-1].period)
        else:
            print(self.name + ' for ' + self.generator + ' Triggered but no plot found')

    def parseNotification(self, data: bytearray):
        # Returns a list of curves to be added to the plot. 
        # The number of curves is determined from the number of SampleShape objects in the data_shape list
        #data = bytearray([0,0,0,0,0,16,2,0,48,4,0,80,6,0,112,8,255,255,255,255])
        #data = bytearray.fromhex('61c90200f8cffff52ffff5cfff031000fa7ffff6afff0140000d90011d50010370fffc8f00fe4ffed54ffe01')
        #print(data[0:4].hex())
        dataBytes = data[self.parse_bytes[0]:self.parse_bytes[1]]
        #print(data.hex())
        aux_val = int.from_bytes(dataBytes, byteorder='big', signed=False) # int representation of all the bits in the notification data
        cycle_size = sum([sample._size_bits for sample in self.data_shape])
        cycle_vals = self._splitBits(aux_val, cycle_size) # contains a list of ints that represents the bits on each cycle
        if not cycle_vals:
            cycle_vals.append(0)
        curves = []
        for curve in self.curves_dict:
            curves.append([])
        # Now we iterate through cycles to parse each cycle
        for cycle in cycle_vals:
            cycle_bits = int2ba(cycle, length=cycle_size, endian='big')
            idx=0
            for shape in self.data_shape:
                for sample_no in range(shape.no_samples):
                    sample  = cycle_bits[idx:idx+shape.resolution]
                    if shape.little_endian:
                        sample.bytereverse()
                        sample.reverse()
                    curves[self.curves_dict[shape.id]].append(ba2int(sample, signed = shape.signed))
                    idx += shape.resolution  
                #print(curves[0])
        return curves

    def _splitBits(self, value, n):
        # Splits an int into a list containing int representation of bit splits
        mask, parts = (1 << n) - 1, []
        parts = []
        while value:
            parts.append(value & mask)
            value >>= n
        parts.reverse()
        return parts

    def setNewShape(self, param_list:list):
        self.data_shape = []
        for params in param_list:
            self.data_shape.append(SampleShape(params[0], params[1], params[2], params[3], params[4], params[5]))
        self.plotitem.clear()
        self.setPlot(self.plotitem)
        return True

@baseTask.register_subclass('Download Session')
class DownloadSessionTask(OngoingTask):
    def __init__(self, client, file_name):
        super().__init__(client, dnld_session_task_sequence, sibel_shrd_stream_uuid)
        self.name = 'Download Session Task'
        self.crc32 = Crc32()
        self.shrd_ind = 0
        self.header = "00"
        self.file_name = file_name
        i = 1
        while exists(self.file_name + ".txt"):
            i += 1
            self.file_name = self.file_name + '-' + str(i)
        with open(self.file_name + ".txt", mode = 'w') as f:
            lines = [
                f"File Name: {self.file_name}",
                f"Write Time: {str(time.time())}",
                f"Client Name: {self.client.Name}",
                f"Client Address: {self.client.address}",
                f"Characteristic UUID: {sibel_shrd_stream_uuid}",
            ]
            f.write("\n".join(lines))
        self.done = False
        self.filestream = None
    
    def trigger(self, data:bytearray):
        self.header = format(data[:1].hex())
        if(self.header == "0f"): # First page (Metadata)
            self.shrd_ind += 1
            self.filestream = open(self.file_name + "_ses-" + str(self.shrd_ind) + ".shrd", mode='ab')
            self.session_crc32 = 0
            print('Received SHRD header')
        elif(self.header == 'ff'): # Data pages
            #print('Received SHRD data page')
            self.filestream.write(data[1:])
            self.session_crc32 = self.crc32.append(self.session_crc32, bytearray(data), 1, len(data) - 1)
        elif(self.header == '11'): # CRC request
            print('Received SHRD CRC')
            try:
                self.filestream.close()
            except:
                print('No filestream to close')
            self.sequence = [
            ('w', sibel_cmd_uuid, bytearray.fromhex("05") + self.session_crc32.to_bytes(4, 'little')),
            ]
            if data[1:].hex() != self.session_crc32.to_bytes(4, 'little').hex():
                print('CRC32 does not match')
            cont_task = copy(self)
            self.continue_with = cont_task
            self.done = True # setting done after the copy so the copy also continues indefinetly
        elif(self.header == '10'): # End of session/Download done
            print('Received SHRD end of session')
            # if(self.ParsingSHRD):
            #     # Parse SHRD
            #     print("Parsing and plotting SHRD")
            #     ParsedData = ShrdParser(os.path.join(desktop_dir, self.file_name + "-" + str(self.shrd_ind) + ".shrd"))
            #     toPlot = ParsedData.parse_all()
            #     number_types = len(toPlot)
            #     for datatype in toPlot:
            #         cur_ax = plt.subplot(number_types,1,toPlot.index(datatype)+1)
            #         for ch in datatype.data:
            #             cur_ax.plot(datatype.data[ch], label=ch.name)
            #     plt.legend()
            #     plt.show()
            try:
                self.filestream.close()
            except:
                print('No filestream to close')
            self.sequence = end_dnld_session_task_sequence
            self.done = True # setting done before the copy so the next iteration does not continue indefinetly
            cont_task = copy(self)
            self.continue_with = cont_task
            #self.reExecute()
            print('Final execution')

    @Task.result.setter
    def result(self, res:bool):
        if self.header == "00":
            if res:
                res = 'Downloading'
            else:
                res = 'Not Downloading'
            Task.result.fset(self, res)    
        elif self.header == '11':
            if res:
                res = 'Sending CRC32 for new session'
            else:
                res = 'Error receiving CRC32'
            Task.result.fset(self, res)  
        elif self.header == '10':
            if not res:
                res = 'Finished Download'
            else:
                res = 'Error during download'
            Task.result.fset(self, res)

@baseTask.register_subclass('Timer') 
class TimerTask(Task):
    # This is not executed. It is just waited for when executing a test plan
    def __init__(self, client, time):
        super().__init__(client, [('t', None, None)])
        self.name = 'Timer Task'
        self.time = time

    @Task.result.setter
    def result(self, res:bool):
        if res:
            res = 'Running timer for ' + str(self.time) + ' secs'
        else:
            res = 'Error running timer'
        Task.result.fset(self, res)

class SampleShape():
    # Defines the shape to parse a number of bytes from the stream buffer
    # A list of this determines a single cycles that is repeated to parse the whole notification data
    def __init__(self, id='Tick', no_samples=1, resolution=32, is_signed=False, is_little_endian=True, sampling_freq=1):
        self.id = id
        self.no_samples = no_samples
        self.resolution = resolution
        self._size_bits = self.no_samples * self.resolution # total size in bits
        self.signed = is_signed
        self.little_endian = is_little_endian
        self.period = 1/sampling_freq
