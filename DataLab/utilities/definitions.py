import sys
import os

import numpy as np
import platform
import asyncio

#import bleak
# from bleak.backends.client import BaseBleakClient
# from bleak.backends.characteristic import BleakGATTCharacteristic

if platform.system() == "Windows":
    desktop_dir = os.path.join("c:/", (os.environ["HOMEPATH"]), "Desktop")
else:
    desktop_dir = os.path.join(os.path.join(os.path.expanduser('~')), 'Desktop')

class Crc32:
    poly = 0xedb88320
    def __init__(self):
        self.table = [0] * 16 * 256
        for i in range(256):
            res = i
            for t in range(16):
                for k in range(8):
                    res = self.poly ^ (res >> 1) if res & 1 == 1 else (res >> 1)
                    self.table[t * 256 + i] = res
    def append(self, crc, input, offset, length):
        crc_local = 0xFFFFFFFF ^ crc
        while(length >= 16):
            a = self.table[(3 * 256) + input[offset + 12]] ^ self.table[(2 * 256) + input[offset + 13]] ^ self.table[(1 * 256) + input[offset + 14]] ^ self.table[(0 * 256) + input[offset + 15]]
            b = self.table[(7 * 256) + input[offset + 8]] ^ self.table[(6 * 256) + input[offset + 9]] ^ self.table[(5 * 256) + input[offset + 10]] ^ self.table[(4 * 256) + input[offset + 11]]
            c = self.table[(11 * 256) + input[offset + 4]] ^ self.table[(10 * 256) + input[offset + 5]] ^ self.table[(9 * 256) + input[offset + 6]] ^ self.table[(8 * 256) + input[offset + 7]]
            d = self.table[(15 * 256) + (crc_local ^ input[offset]) % 256] ^ self.table[(14 * 256) + ((crc_local >> 8) ^ input[offset + 1]) % 256] ^ self.table[(13 * 256)+ ((crc_local >> 16) ^ input[offset + 2]) % 256] ^ self.table[(12 * 256) + ((crc_local >> 24) ^ input[offset+3]) % 256]
            crc_local = d ^ c ^ b ^ a
            offset += 16
            length -= 16
        length -= 1
        while(length >= 0):
            crc_local = self.table[(crc_local ^ input[offset]) % 256] ^ crc_local >> 8
            offset += 1
            length -= 1
        return crc_local ^ 0xFFFFFFFF

class AFE():
    def __init__(self, type, ptr):
        #super().__init__
        self.type = type #int
        self.registers = {}
        self.ptr = ptr #bytearray

    def addRegister(self, reg_add, val):
        self.registers[reg_add] = val #[int]bytearray


hmon_arg_split = "HMONARG"
sibel_ppg_stream_uuid = '05791401-78c9-481e-b5ee-1866f83eda7b'
sibel_ppg_CMD_uuid = '05791402-78c9-481e-b5ee-1866f83eda7b'
sibel_ecg_stream_uuid = '05791101-78c9-481e-b5ee-1866f83eda7b'
sibel_accl_stream_uuid = '05791301-78c9-481e-b5ee-1866f83eda7b'
sibel_bioz_stream_uuid = '05791701-78c9-481e-b5ee-1866f83eda7b'
sibel_temp_stream_uuid = '05791501-78c9-481e-b5ee-1866f83eda7b'
sibel_shrd_stream_uuid = '05791003-78c9-481e-b5ee-1866f83eda7b'
sibel_cmd_uuid = '05791002-78c9-481e-b5ee-1866f83eda7b'
sibel_sys_stream = '05791001-78c9-481e-b5ee-1866f83eda7b'
sible_security_uuit = '05791901-78c9-481e-b5ee-1866f83eda7b'


#TODO rename and include all pertinent characteristics
sibel_uuid = {
    sibel_ppg_stream_uuid: 'Sibel PPG Stream',
    sibel_ppg_CMD_uuid: 'Sibel PPG CMD',
    sibel_temp_stream_uuid: 'Sibel Temp Stream',
    sibel_bioz_stream_uuid: 'Sibel BioZ Stream',
    sibel_ecg_stream_uuid: 'Sibel ECG Stream',
    sibel_accl_stream_uuid: 'Sibel Accl Stream',
    sible_security_uuit: 'Sibel Security',
    sibel_sys_stream: 'Sibel System Stream',
    sibel_cmd_uuid: 'Sibel System CMD',
    sibel_shrd_stream_uuid: 'Sibel SHRD Stream',
    sibel_ppg_CMD_uuid: 'Sibel PPG CMD',
}


battery_task_sequence = [
    ('r','00002a19-0000-1000-8000-00805f9b34fb',None),
]
dlinfo_task_sequence = [
    ('w', sibel_cmd_uuid, bytearray.fromhex('03')),
    ('r', sibel_sys_stream, None)
]
stop_session_task_sequence = [
    ('w', sibel_cmd_uuid, bytearray.fromhex('02')),
    ('r', sibel_sys_stream, None)
]
dnld_session_task_sequence = [
    ('n', sibel_shrd_stream_uuid, None),
    ('n', sibel_sys_stream, None),
    ('w', sibel_cmd_uuid, bytearray.fromhex('02')),
    ('w', sibel_cmd_uuid, bytearray.fromhex('04')),
    ('h', None, None)
]
end_dnld_session_task_sequence = [
    ('n', sibel_shrd_stream_uuid, None),
    ('n', sibel_sys_stream, None)
]

session_status_descriptor = {
    0x00: 'No session started',
    0x01: 'Session started and ongoing',
    0x02: 'Session started, not ongoing',
    0xFF: 'Reached max session number',
    0xFE: 'Memory limit'
}

device_type = {
    0x00 : 'MAX30001',
    #0x01 : 'MAX30205'
}

device_regs = {
    0x00: [0x10,0x12,0x14,0x15,0x17,0x18,0x1A,0x1D,0x1E],
    #0x01: [0x01,0x02,0x03,0x04]
}