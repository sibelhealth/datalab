from bleak import BleakClient, BleakScanner, discover
from bleak.backends.characteristic import BleakGATTCharacteristic
from bleak.backends.client import BaseBleakClient
from bleak.backends.device import BLEDevice

from datalab.utilities.definitions import *


#import traceback
import asyncio

from PyQt5.QtCore import QObject, QThread, pyqtSignal


class BLEClient(BleakClient):
    def __init__(self,device, disconnect_callback):
        super().__init__(device,disconnected_callback=disconnect_callback)
        self.Name = ''
        self.managing_service = None
        self.notifying_characteristics = {}
        self.is_DL_compatible = True

    async def start_notify(self, char:BleakGATTCharacteristic):
        self.notifying_characteristics[char] = BLENotifier(self.managing_service, self, char)
        await super().start_notify(char, self.notifying_characteristics[char].newNotification)
        #await super().start_notify(char, self.test)

    async def stop_notify(self, char):
        await super().stop_notify(char)
        self.notifying_characteristics.pop(char)

    def notify(self, char, data):
        self.managing_service.controller.getNotification(self, char, data)

class BLENotifier:
    def __init__(self, service, client:BLEClient, char:BleakGATTCharacteristic):
        self.client = client
        self.char = char

    def newNotification(self, sender, data):
        data = bytearray(data) # Avoid potential bug in MAC
        self.client.notify(self.char, data)

class BLEService:
    def __init__(self, caller):
        self.controller = caller
        self.ScannedDevices = {}
        self.ConnectedClients = []
        self.graph_clients = {}

    def onClientDisconnected(self, client):
        if client in self.ConnectedClients:
            self.ConnectedClients.remove(client)
        self.controller.disconnectedClient(client)

    def scanDevices(self):
        try:
            loop = asyncio.get_event_loop()
            devices = loop.run_until_complete(asyncScanDevices())
            #self.ScannedDevices.clear()
            device : BLEDevice
            for device in devices:
                if device.name:
                    self.ScannedDevices[device.address] = device
            return True
        except:
            return False

    def connectDevice(self, address, name):
        # loop = asyncio.new_event_loop()
        # asyncio.set_event_loop(loop) 
        loop = asyncio.get_event_loop()
        client:BLEClient
        client, success = loop.run_until_complete(asyncConnectDevice(address, self.onClientDisconnected))
        if(success):
            client : BLEClient
            client.Name = name
            client.managing_service = self
            # The managing service of a BLEClient must define a 'reportNotification' function to accept notifications
            self.ConnectedClients.append(client)
            dl_service = client.services.get_service('05791000-78c9-481e-b5ee-1866f83eda7b') # Get DataLab compatibility
            if dl_service:
                client.is_DL_compatible = True
        self.ScannedDevices.clear()
        return client, success

    def disconnectDevice(self, client):
        # loop = asyncio.new_event_loop()
        # asyncio.set_event_loop(loop) 
        loop = asyncio.get_event_loop()
        success = loop.run_until_complete(asyncDisconnectDevice(client))
        if success:
            if client in self.ConnectedClients:
                self.ConnectedClients.remove(client)
            return success
        else:
            return False

    def writeChar(self, client:BLEClient, characteristic:BleakGATTCharacteristic, data:bytearray):
        # loop = asyncio.new_event_loop()
        # asyncio.set_event_loop(loop) 
        loop = asyncio.get_event_loop()
        ret = loop.run_until_complete(asyncWrite(client, characteristic, data))
        return ret

    def readChar(self, client:BLEClient, characteristic:BleakGATTCharacteristic):
        # loop = asyncio.new_event_loop()
        # asyncio.set_event_loop(loop)
        loop = asyncio.get_event_loop()
        ret = loop.run_until_complete(asyncRead(client, characteristic))
        return ret

    def toggleNotification(self, client: BLEClient, char:BleakGATTCharacteristic):
        if(char not in client.notifying_characteristics.keys()):
            # loop = asyncio.new_event_loop()
            # asyncio.set_event_loop(loop)
            loop = asyncio.get_event_loop()
            loop.run_until_complete(asyncStartNotify(client, char))
            if(char in client.notifying_characteristics.keys()):
                return True
            else:
                loop = asyncio.new_event_loop()
                asyncio.set_event_loop(loop)
                loop = asyncio.get_event_loop()
                loop.run_until_complete(asyncStopNotify(client, char))
                return False
        else:
            loop = asyncio.new_event_loop()
            asyncio.set_event_loop(loop)
            loop = asyncio.get_event_loop()
            loop.run_until_complete(asyncStopNotify(client, char))
            return False

    def reportNotification(self, client, char, data):
        # TODO organize data as a producer consumer. For now, only report back to controller to print in log
        self.controller.reportNotify(client, char, data)

async def asyncScanDevices():
    devices = await discover(1)
    return devices

async def asyncWrite(client:BaseBleakClient, char:BleakGATTCharacteristic, data:bytearray):
    try:
        await client.write_gatt_char(char, data)
        return True
    except:
        traceback.print_exc()
        return False

async def asyncRead(client:BLEClient, char:BleakGATTCharacteristic):
    try:
        ret = await client.read_gatt_char(char)
        return ret
    except:
        traceback.print_exc()
        return False

async def asyncConnectDevice(address, disconnect_callback):
    try:
        device = await BleakScanner.find_device_by_address(address)
        client = BLEClient(device, disconnect_callback)
        await client.connect(timeout=10.0, use_cached=False)
        x = await client.is_connected()
        if(x):
            svcs = await client.get_services()
        return client, x
    except:
        return None, False

async def asyncDisconnectDevice(client: BLEClient):
    try:
        await client.disconnect()
        return True
    except:
        return False

async def asyncStartNotify(client:BLEClient, char:BleakGATTCharacteristic):
    try:
        await client.start_notify(char) # Modified method that does not accept the callback arg
        return True
    except:
        return False

async def asyncStopNotify(client:BLEClient, char:BleakGATTCharacteristic):
    try:
        await client.stop_notify(char)
        return True
    except:
        return False

if __name__== '__main__':
    pass